package org.university.data.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.university.data.configuration.ProjectConfig;
import org.university.data.model.College;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ProjectConfig.class})
public class CollegeDAOHibernateTest {

    @Autowired
    private CollegeDAO collegeDao;

    @Before
    public void initialize() {
        collegeDao.insert("College 1", "Main info");
        collegeDao.insert("College 2", "Main info");
        collegeDao.insert("College 3", "Main info");
    }
    @Test
    public void insert() {
        Integer sizeBefore = collegeDao.getList().size();
        assertEquals(new College(4, "College 4", "Main info"), collegeDao.insert("College 4", "Main info"));
        Integer sizeAfter = collegeDao.getList().size();
        assertEquals(1, sizeAfter - sizeBefore);
    }

    @Test
    public void update() {
        assertEquals(new College(1,"College 1", "Did you remember all?"), collegeDao.update(new College(1,"College 1", "Did you remember all?")));
        assertNotEquals(new College(1,"College 1", "Did you remember all?"),  collegeDao.update(new College(1,"College 1", "Main info")));

    }

    @Test
    public void getList() {
        assertTrue(collegeDao.getList().size() >= 3);
        assertFalse(collegeDao.getList().isEmpty());
    }

    @Test
    public void get() {
        assertEquals(new College(2,"College 2", "Main info"), collegeDao.get(2));
        assertEquals(null, collegeDao.get(30));
    }

    @Test
    public void deleteById() {
        Integer sizeBefore = collegeDao.getList().size();
        assertEquals("College 1", collegeDao.deleteById(1).getName());
        Integer sizeAfter = collegeDao.getList().size();
        assertEquals(1, sizeBefore - sizeAfter);
    }

    @Test
    public void nameColleges() {
        assertTrue(collegeDao.nameColleges().containsValue("College 1"));
        assertTrue(collegeDao.nameColleges().containsValue("College 2"));
        assertTrue(collegeDao.nameColleges().containsValue("College 3"));
        assertFalse(collegeDao.nameColleges().containsValue("College 5"));
    }
}
