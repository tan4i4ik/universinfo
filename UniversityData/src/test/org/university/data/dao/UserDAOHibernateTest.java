package org.university.data.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.university.data.configuration.ProjectConfig;
import org.university.data.model.User;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ProjectConfig.class})
public class UserDAOHibernateTest {

    @Autowired
    private UserDAO userDAO;

    @Test
    public void insert() {
        assertEquals(new User( "user4", "some name", "ttt@dd.dd", "ddddd"),userDAO.insert("user4", "some name", "ttt@dd.dd", "ddddd"));
    }

    @Test
    public void getUserByLogin() {
        userDAO.insert("user1", "name", "ttt@dd.dd", "");
        assertEquals(new User( "user1", "name", "ttt@dd.dd", ""),userDAO.getUserByLogin("user1"));
        assertEquals(null,userDAO.getUserByLogin("user7"));
    }
}
