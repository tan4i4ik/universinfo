package org.university.data.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.university.data.configuration.ProjectConfig;
import org.university.data.model.College;
import org.university.data.model.Course;
import org.university.data.model.Teacher;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ProjectConfig.class})
public class CourseDAOHibernateTest {

    @Autowired
    private CourseDAO courseDao;

    @Autowired
    private TeacherDAO teacherDao;

    @Autowired
    private CollegeDAO collegeDao;

    Teacher teacher;

    @Before
    public void initialize(){
        College college = collegeDao.insert("College 1", "Main info");
        teacher = teacherDao.insert("Jorge Tomson", "professor 1", college);
        courseDao.insert("Course 1","desc", teacher);
        courseDao.insert("Course 2","desc", teacher);
        courseDao.insert("Course 3","desc", teacher);

    }
    @Test
    public void insert() {
        Integer beforeSize = courseDao.getList().size();
        assertEquals(new Course(4,"Course 4","desc", teacher), courseDao.insert("Course 4","desc", teacher));
        Integer afterSize = courseDao.getList().size();
        assertEquals(1, afterSize - beforeSize);
    }

    @Test
    public void get() {
        assertEquals(new Course(3,"Course 3","desc", teacher), courseDao.get(3));
        assertEquals(null, courseDao.get(50));
    }

    @Test
    public void update() {
      assertEquals(new Course(1,"Course 7","desc", teacher), courseDao.update(new Course(1,"Course 7","desc", teacher)));
      assertNotEquals(new Course(2,"Course 2","desc", teacher), courseDao.update(new Course(2,"Course 10","desc", teacher)));
    }

    @Test
    public void getList() {
        assertTrue(courseDao.getList().size() >= 3);
        assertFalse(courseDao.getList().isEmpty());
    }

    @Test
    public void getCoursesByTeacherId() {
        assertTrue(courseDao.getCoursesByTeacherId(1).size() >= 2);
        assertFalse(courseDao.getCoursesByTeacherId(2) == null);
    }

    @Test
    public void deleteById() {
        Integer sizeBefore = courseDao.getList().size();
        assertEquals("Course 3", courseDao.deleteById(3).getName());
        Integer sizeAfter = courseDao.getList().size();
        assertEquals(1, sizeBefore - sizeAfter);
    }
}
