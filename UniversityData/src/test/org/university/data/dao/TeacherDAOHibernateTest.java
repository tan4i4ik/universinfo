package org.university.data.dao;

import org.junit.Before;
import org.university.data.configuration.ProjectConfig;
import org.university.data.model.College;
import org.university.data.model.Teacher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ProjectConfig.class})
public class TeacherDAOHibernateTest {

    @Autowired
    private TeacherDAO teacherDao;

    @Autowired
    private CollegeDAO collegeDao;

    College college;

    @Before
    public void initialize(){
        college = collegeDao.insert("College 1", "Main info");
        teacherDao.insert("professor 1","professor 1", college);
        teacherDao.insert("professor 2","professor 1", college);
        teacherDao.insert("professor 3","professor 1", college);
    }

    @Test
    public void insert() {
        Integer beforeSize = teacherDao.getList().size();
        assertEquals(new Teacher(4,"professor 4","professor 4", college),teacherDao.insert("professor 4","professor 4", college));
        Integer afterSize = teacherDao.getList().size();
        assertEquals(1, afterSize - beforeSize);
    }

    @Test
    public void update() {
        assertEquals(new Teacher(1,"professor 5","professor 5", college), teacherDao.update( new Teacher(1,"professor 5","professor 5", college)));
        assertNotEquals(new Teacher(1,"professor 5","professor 5", college), teacherDao.update( new Teacher(1,"professor 6","professor 6", college)));
    }

    @Test
    public void deleteById() {
        Integer sizeBefore = teacherDao.getList().size();
        assertEquals("professor 2", teacherDao.deleteById(2).getName());
        Integer sizeAfter = teacherDao.getList().size();
        assertEquals(1, sizeBefore - sizeAfter);
    }

    @Test
    public void getList() {
        assertTrue(teacherDao.getList().size() >= 3);
    }

    @Test
    public void get() {
        assertEquals(new Teacher(3,"professor 3","professor 1", college), teacherDao.get(3));
        assertEquals(null, teacherDao.get(50));
    }

    @Test
    public void nameTeachers() {
        assertTrue(teacherDao.nameTeachers().containsValue("professor 3"));
        assertFalse(teacherDao.nameTeachers().containsValue("professor 10"));
    }

    @Test
    public void getTeachersByCollege() {
        assertTrue(teacherDao.getTeachersByCollegeId(1).size() > 0);
        assertTrue(teacherDao.getTeachersByCollegeId(10).isEmpty());
    }
}
