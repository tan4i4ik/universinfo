package org.university.data.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.university.data.configuration.ProjectConfig;
import org.university.data.model.Student;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ProjectConfig.class})
public class StudentDAOHibernateTest {
    
    @Autowired
    private StudentDAO studentDao;

    @Before
    public void initialize() {
        studentDao.insert("Student 1");
        studentDao.insert("Student 2");
        studentDao.insert("Student 3");
    }
    
    @Test
    public void insert() {
        Integer beforeSize = studentDao.getList().size();
        assertEquals(new Student(4,"Student 4"), studentDao.insert("Student 4"));
        Integer afterSize = studentDao.getList().size();
        assertEquals(1, afterSize - beforeSize);
    }

    @Test
    public void update() {
        assertEquals(new Student(1, "Student 5"), studentDao.update(new Student(1,"Student 5")));
        assertNotEquals(new Student(1, "Student 5"), studentDao.update(new Student(1,"Student 6")));
    }

    @Test
    public void deleteById() {
        Integer sizeBefore = studentDao.getList().size();
        assertEquals("Student 2", studentDao.deleteById(2).getName());
        Integer sizeAfter = studentDao.getList().size();
        assertEquals(1, sizeBefore - sizeAfter);
    }

    @Test
    public void getList() {
        assertTrue(studentDao.getList().size() >= 1);
        assertFalse(studentDao.getList().isEmpty());
    }

    @Test
    public void getStudentByName() {
        assertEquals(new Student(3, "Student 3"), studentDao.getByName("Student 3"));
    }

    @Test
    public void getStudentList() {
        assertTrue(studentDao.getByStudentName("Stud").size() >= 2);
        assertTrue(studentDao.getByStudentName("Some") == null);
    }
}
