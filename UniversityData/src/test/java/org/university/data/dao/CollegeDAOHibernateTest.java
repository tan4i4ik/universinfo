package org.university.data.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.university.data.configuration.ProjectConfig;
import org.university.data.model.College;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ProjectConfig.class})
public class CollegeDAOHibernateTest {

    @Autowired
    private CollegeDAO collegeDao;

    @Before
    public void initialize() {
        collegeDao.insert("College 1", "Main info");
        collegeDao.insert("College 2", "Main info");
    }
    @Test
    public void insert() {
        College college = collegeDao.insert("College 1", "Main info");
        assertEquals("College 1", college.getName());
    }

    @Test
    public void update() {
        assertEquals(new College(2,"College 2", "Did you remember all?"), collegeDao.update(new College(2,"College 2", "Did you remember all?")));
        assertNotEquals(new College(2,"College 2", "Did you remember all?"),  collegeDao.update(new College(2,"College 2", "Main info")));

    }

    @Test
    public void getList() {
        assertTrue(collegeDao.getList().size() >= 3);
        assertFalse(collegeDao.getList().isEmpty());
    }

    @Test
    public void get() {
        assertEquals("College 1", collegeDao.get(1).getName());
        assertEquals(null, collegeDao.get(30));
    }

    @Test
    public void deleteById() {
        Integer beforeSize = collegeDao.getList().size();
        College college = collegeDao.deleteById(2);
        Integer afterSize = collegeDao.getList().size();
        assertEquals(beforeSize- afterSize, 1);
    }

    @Test
    public void nameColleges() {
        assertTrue(collegeDao.nameColleges().containsValue("College 1"));
        assertTrue(collegeDao.nameColleges().containsValue("College 2"));
        assertFalse(collegeDao.nameColleges().containsValue("College 5"));
    }

}
