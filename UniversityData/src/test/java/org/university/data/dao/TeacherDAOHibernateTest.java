package org.university.data.dao;

import org.junit.Before;
import org.university.data.configuration.ProjectConfig;
import org.university.data.model.College;
import org.university.data.model.Teacher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ProjectConfig.class})
public class TeacherDAOHibernateTest {

    @Autowired
    private TeacherDAO teacherDao;

    @Autowired
    private CollegeDAO collegeDao;

    private College college;
    private Integer teacherIdDelete;
    private Integer teacherIdGet;

    @Before
    public void initialize(){
        college = collegeDao.insert("College 1", "Main info");
        teacherIdDelete = teacherDao.insert("Professor 1","Professor 1", college).getId();
        teacherDao.insert("Professor 2","Professor 2", college);
        teacherIdGet = teacherDao.insert("Professor 3","Professor 3", college).getId();
    }

    @Test
    public void insert() {
        assertEquals("Professor 4",teacherDao.insert("Professor 4","professor 4", college).getName());
    }

    @Test
    public void update() {
        assertEquals(new Teacher(2,"professor 5","professor 5", college), teacherDao.update( new Teacher(2,"professor 5","professor 5", college)));
        assertNotEquals(new Teacher(2,"professor 5","professor 5", college), teacherDao.update( new Teacher(2,"professor 6","professor 6", college)));
    }

    @Test
    public void deleteById() {
        assertEquals("Professor 1", teacherDao.deleteById(teacherIdDelete).getName());
    }

    @Test
    public void getList() {
        assertTrue(teacherDao.getList().size() >= 3);
    }

    @Test
    public void get() {
        assertEquals(new Teacher(teacherIdGet,"Professor 3","Professor 3", college), teacherDao.get(teacherIdGet));
        assertEquals(null, teacherDao.get(50));
    }

    @Test
    public void nameTeachers() {
        assertTrue(teacherDao.nameTeachers().containsValue("Professor 3"));
        assertFalse(teacherDao.nameTeachers().containsValue("professor 10"));
    }

    @Test
    public void getTeachersByCollege() {
        assertTrue(teacherDao.getTeachersByCollegeId(college.getId()).size() > 0);
        assertTrue(teacherDao.getTeachersByCollegeId(10).isEmpty());
    }
}
