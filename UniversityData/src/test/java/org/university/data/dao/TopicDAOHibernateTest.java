package org.university.data.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.university.data.configuration.ProjectConfig;
import org.university.data.model.College;
import org.university.data.model.Course;
import org.university.data.model.Teacher;
import org.university.data.model.Topic;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ProjectConfig.class})
public class TopicDAOHibernateTest {

    @Autowired
    private TopicDAO topicDao;

    @Autowired
    private CourseDAO courseDao;

    @Autowired
    private TeacherDAO teacherDao;

    @Autowired
    private CollegeDAO collegeDao;

    private Course course;
    private int idTopicDelete;
    private int idTopicGet;
    private int idTopicInsert;

    @Before
    public void initialize(){
        College college = collegeDao.insert("College 1", "Main info");
        Teacher teacher = teacherDao.insert("professor 1", "professor 1", college);
        Course course = courseDao.insert("Course 1", "desc", teacher);
        idTopicGet = topicDao.insert("Topic 1", "desc", course).getId();
        idTopicDelete = topicDao.insert("Topic 2", "desc", course).getId();
        idTopicInsert = topicDao.insert("Topic 3", "desc", course).getId();
    }

    @Test
    public void insert() {
        assertEquals(new Topic(idTopicInsert + 1,"Topic 4","desc", course), topicDao.insert("Topic 4","desc", course));
    }

    @Test
    public void get() {
        assertEquals("Topic 1", topicDao.get(idTopicGet).getName());
        assertEquals(null, topicDao.get(50));
    }

    @Test
    public void update() {
        assertEquals(new Topic(1,"Topic 7","desc", course), topicDao.update(new Topic(1,"Topic 7","desc", course)));
        assertNotEquals(new Topic(1,"Course 1","desc", course), topicDao.update(new Topic(1,"Course 10","desc", course)));
    }

    @Test
    public void getList() {
        assertTrue(topicDao.getList().size() >= 3);
        assertFalse(topicDao.getList().isEmpty());
    }

    @Test
    public void deleteById() {
        assertEquals("Topic 2", topicDao.deleteById(idTopicDelete).getName());
    }

}
