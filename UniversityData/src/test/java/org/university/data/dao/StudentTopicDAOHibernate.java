package org.university.data.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.university.data.configuration.ProjectConfig;
import org.university.data.model.*;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ProjectConfig.class})
public class StudentTopicDAOHibernate {

    @Autowired
    private StudentTopicDAO studentTopicDao;

    @Autowired
    private StudentDAO studentDao;

    @Autowired
    private CollegeDAO collegeDao;

    @Autowired
    private TeacherDAO teacherDao;

    @Autowired
    private CourseDAO courseDao;

    @Autowired
    private TopicDAO topicDao;

    Topic topic;
    Student student;

    @Before
    public void initialize() {
        College college = collegeDao.insert("College 1", "desc");
        Teacher teacher = teacherDao.insert("Teacher 1", "desc", college);
        Course course = courseDao.insert("Course 1", "desc", teacher);
        topic = topicDao.insert("Topic 1", "desc", course);
        student = studentDao.insert("Student 1");
        studentTopicDao.insert(student,topic,5);
        studentTopicDao.insert(student, topic,6);
    }

    @Test
    public void insert() {
        assertEquals("5", Integer.toString(studentTopicDao.insert(student,topic,1)));
    }

    @Test
    public void delete() {
        assertEquals("6", Integer.toString(studentTopicDao.deleteById(2).getGraded()));
    }
}
