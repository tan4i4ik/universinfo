package org.university.data.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.university.data.serializers.CollegeSerializer;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "colleges")
@JsonSerialize(using = CollegeSerializer.class)
public class College {

    @Id
    @GenericGenerator(name="gen",strategy="increment")
    @GeneratedValue(generator="gen")
    @Column(name = "college_id")
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description", nullable = false)
    private String description;

    @OneToMany(mappedBy = "college", cascade = CascadeType.ALL)
    private List<Teacher> teacherList;

    public College() {}

    public College(Integer id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.teacherList = new ArrayList<>();
    }

    public College(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Teacher> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List<Teacher> teacherList) {
        this.teacherList = teacherList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        College college = (College) o;
        return Objects.equals(id, college.id) &&
                Objects.equals(name, college.name) &&
                Objects.equals(description, college.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }
}
