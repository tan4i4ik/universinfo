package org.university.data.model;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "topic_students")
public class StudentTopic implements Serializable {

    @Id
    @GenericGenerator(name="gen",strategy="increment")
    @GeneratedValue(generator="gen")
    @Column(name = "topic_student_id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "topic_id")
    private Topic topic;

    @Column(name = "graded")
    private Integer graded;

    public StudentTopic(Student student,Topic topic, Integer graded) {
        this.student = student;
        this.topic = topic;
        this.graded = graded;
    }

    public StudentTopic(Integer id, Student student, Topic topic, Integer graded) {
        this.id = id;
        this.student = student;
        this.topic = topic;
        this.graded = graded;
    }

    public StudentTopic(){}

    @Transient
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Transient
    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public Integer getGraded() {
        return graded;
    }

    public void setGraded(Integer graded) {
        this.graded = graded;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentTopic that = (StudentTopic) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(student, that.student) &&
                Objects.equals(topic, that.topic) &&
                Objects.equals(graded, that.graded);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, student, topic, graded);
    }
}