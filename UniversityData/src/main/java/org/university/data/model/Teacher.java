package org.university.data.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.university.data.serializers.TeacherSerializer;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "teachers")
@JsonSerialize(using = TeacherSerializer.class)
public class Teacher {

    @Id
    @GenericGenerator(name="gen",strategy="increment")
    @GeneratedValue(generator="gen")
    @Column(name = "teacher_id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "college_id")
    private College college;

    @OneToMany(mappedBy = "teacher", cascade = CascadeType.ALL)
    private List<Course> courseList;

    public Teacher() {}

    public Teacher(Integer id, String name, String description, College college) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.college = college;
    }

    public Teacher(Integer id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Teacher(Integer id, String name, String description, College college, List<Course> list) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.college = college;
        this.courseList = list;
    }

    public Teacher(String name, String description, College college) {
        this.name = name;
        this.description = description;
        this.college = college;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }

    public List<Course> getCourseList() {
        return this.courseList;
    }

    public void setCourseList(List<Course> list) {
        this.courseList = list;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return Objects.equals(id, teacher.id) &&
                Objects.equals(name, teacher.name) &&
                Objects.equals(description, teacher.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }
}
