package org.university.data.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "users")
public class User {

        @Id
        @Column(name = "username", nullable = false, unique = true)
        private String username;

        @Column(name = "full_name", nullable = false)
        private String fullName;

        @Column(name = "email", nullable = false)
        private String email;

        @Column(name = "password", nullable = false)
        private String password;

        public User() {}

        public User(String username, String fullName, String email, String password) {
                this.username = username;
                this.fullName = fullName;
                this.email = email;
                this.password = password;
        }

        public String getUsername() {
                return username;
        }

        public void setUsername(String username) {
                this.username = username;
        }

        public String getFullName() {
                return fullName;
        }

        public void setFullName(String fullName) {
                this.fullName = fullName;
        }

        public String getEmail() {
                return email;
        }

        public void setEmail(String email) {
                this.email = email;
        }

        public String getPassword() {
                return password;
        }

        public void setPassword(String password) {
                this.password = password;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                User user = (User) o;
                return Objects.equals(username, user.username) &&
                        Objects.equals(fullName, user.fullName) &&
                        Objects.equals(email, user.email) &&
                        Objects.equals(password, user.password);
        }

        @Override
        public int hashCode() {

                return Objects.hash( username, fullName, email, password);
        }
}
