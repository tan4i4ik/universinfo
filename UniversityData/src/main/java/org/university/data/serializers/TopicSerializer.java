package org.university.data.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.university.data.model.Topic;

import java.io.IOException;

public class TopicSerializer extends JsonSerializer<Topic> {

    @Override
    public void serialize(Topic topic, JsonGenerator jsonGenerator, SerializerProvider serializers) throws IOException, JsonProcessingException {

        jsonGenerator.writeStartObject();
        jsonGenerator.writeFieldName("topic_id");
        jsonGenerator.writeNumber(topic.getId());
        jsonGenerator.writeFieldName("topic_name");
        jsonGenerator.writeString(topic.getName());
        jsonGenerator.writeFieldName("topic_description");
        jsonGenerator.writeString(topic.getDescription());
        jsonGenerator.writeFieldName("student_list");
        jsonGenerator.writeObject(topic.getListStudents());
        jsonGenerator.writeEndObject();
    }
}