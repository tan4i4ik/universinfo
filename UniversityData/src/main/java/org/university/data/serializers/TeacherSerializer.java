package org.university.data.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.university.data.model.Teacher;

import java.io.IOException;

public class TeacherSerializer extends JsonSerializer<Teacher> {

    @Override
    public void serialize(Teacher teacher, JsonGenerator jsonGenerator, SerializerProvider serializers) throws IOException, JsonProcessingException {

        jsonGenerator.writeStartObject();
        jsonGenerator.writeFieldName("id");
        jsonGenerator.writeNumber(teacher.getId());
        jsonGenerator.writeFieldName("name");
        jsonGenerator.writeString(teacher.getName());
        jsonGenerator.writeFieldName("description");
        jsonGenerator.writeString(teacher.getDescription());
        jsonGenerator.writeFieldName("college_id");
        jsonGenerator.writeNumber(teacher.getCollege().getId());
        jsonGenerator.writeFieldName("college_name");
        jsonGenerator.writeString(teacher.getCollege().getName());
        jsonGenerator.writeFieldName("list");
        jsonGenerator.writeObject(teacher.getCourseList());
        jsonGenerator.writeEndObject();
    }
}

