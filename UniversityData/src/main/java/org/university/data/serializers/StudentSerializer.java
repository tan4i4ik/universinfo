package org.university.data.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.university.data.model.Student;
import org.university.data.model.StudentTopic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentSerializer extends JsonSerializer<Student> {

        @Override
        public void serialize(Student student, JsonGenerator jsonGenerator, SerializerProvider serializers) throws IOException, JsonProcessingException {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeFieldName("id");
                jsonGenerator.writeNumber(student.getId());
                jsonGenerator.writeFieldName("name");
                jsonGenerator.writeString(student.getName());
                jsonGenerator.writeFieldName("subjects");
                jsonGenerator.writeStartArray();
                for (String subject: getCoursesStudent(student.getListTopic()).keySet()) {
                        jsonGenerator.writeStartObject();
                        jsonGenerator.writeFieldName("name");
                        jsonGenerator.writeString(subject);
                        jsonGenerator.writeFieldName("topics");
                        jsonGenerator.writeStartArray();
                        for(StudentTopic studentTopic : getCoursesStudent(student.getListTopic()).get(subject)) {
                                jsonGenerator.writeStartObject();
                                jsonGenerator.writeFieldName("id");
                                jsonGenerator.writeNumber(studentTopic.getId());
                                jsonGenerator.writeFieldName("name");
                                jsonGenerator.writeString(studentTopic.getTopic().getName());
                                jsonGenerator.writeFieldName("grade");
                                jsonGenerator.writeNumber(studentTopic.getGraded());
                                jsonGenerator.writeEndObject();
                        }
                        jsonGenerator.writeEndArray();
                        jsonGenerator.writeEndObject();
                }
                jsonGenerator.writeEndArray();
                jsonGenerator.writeEndObject();
        }

        private Map<String, List<StudentTopic>> getCoursesStudent(List<StudentTopic> listTopic) {
                Map<String,List<StudentTopic>> map = new HashMap<>();
                for (StudentTopic studentTopic: listTopic) {
                        if (!map.containsKey(studentTopic.getTopic().getCourse().getName())) {
                                List<StudentTopic> currList = new ArrayList<>();
                                currList.add(studentTopic);
                                map.put(studentTopic.getTopic().getCourse().getName(), currList );
                        } else {
                                map.get(studentTopic.getTopic().getCourse().getName()).add(studentTopic);
                        }
                }
                return map;
        }
}
