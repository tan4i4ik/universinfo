package org.university.data.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.university.data.model.Course;
import org.university.data.model.StudentTopic;
import org.university.data.model.Topic;

import java.io.IOException;

public class CourseSerializer extends JsonSerializer<Course> {

    @Override
    public void serialize(Course course, JsonGenerator jsonGenerator, SerializerProvider serializers) throws IOException, JsonProcessingException {

        jsonGenerator.writeStartObject();
        jsonGenerator.writeFieldName("id");
        jsonGenerator.writeNumber(course.getId());
        jsonGenerator.writeFieldName("name");
        jsonGenerator.writeString(course.getName());
        jsonGenerator.writeFieldName("description");
        jsonGenerator.writeString(course.getDescription());
        jsonGenerator.writeFieldName("teacher_id");
        jsonGenerator.writeNumber(course.getTeacher().getId());
        jsonGenerator.writeFieldName("teacher_name");
        jsonGenerator.writeString(course.getTeacher().getName());
        jsonGenerator.writeFieldName("topics");
        jsonGenerator.writeStartArray();
        for (Topic topic: course.getTopicList()) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeFieldName("id");
            jsonGenerator.writeNumber(topic.getId());
            jsonGenerator.writeFieldName("name");
            jsonGenerator.writeString(topic.getName());
            jsonGenerator.writeFieldName("description");
            jsonGenerator.writeString(topic.getDescription());
            jsonGenerator.writeFieldName("students");
            jsonGenerator.writeStartArray();
            for (StudentTopic studentTopic: topic.getListStudents()) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeFieldName("id");
                jsonGenerator.writeNumber(studentTopic.getId());
                jsonGenerator.writeFieldName("name");
                jsonGenerator.writeString(studentTopic.getStudent().getName());
                jsonGenerator.writeFieldName("graded");
                jsonGenerator.writeNumber(studentTopic.getGraded());
                jsonGenerator.writeEndObject();
            }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
