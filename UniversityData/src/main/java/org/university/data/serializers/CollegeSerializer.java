package org.university.data.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.university.data.model.College;

import java.io.IOException;

public class CollegeSerializer extends JsonSerializer<College> {

    @Override
    public void serialize(College college, JsonGenerator jsonGenerator, SerializerProvider serializers) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeFieldName("id");
        jsonGenerator.writeNumber(college.getId());
        jsonGenerator.writeFieldName("name");
        jsonGenerator.writeString(college.getName());
        jsonGenerator.writeFieldName("description");
        jsonGenerator.writeString(college.getDescription());
        jsonGenerator.writeFieldName("teacherList");
        jsonGenerator.writeObject(college.getTeacherList());
        jsonGenerator.writeEndObject();
    }
}

