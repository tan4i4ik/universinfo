package org.university.data.configuration;

import org.h2.tools.Server;
import org.university.data.aspects.Aspects;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@ComponentScan("org.university.data")
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class ProjectConfig {

    @Bean(name="dataSource")
    public DataSource dataSource() {
        Properties properties = new Properties();
        properties.setProperty("foreign_keys", "true");
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName("org.h2.Driver");
        driverManagerDataSource.setUrl("jdbc:h2:mem:db;DB_CLOSE_DELAY=-1");
        driverManagerDataSource.setUsername("");
        driverManagerDataSource.setPassword("");
        driverManagerDataSource.setConnectionProperties(properties);

        return driverManagerDataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource());
        return jdbcTemplate;
    }

    @Bean
    public Properties getHibernateProperties() {
        Properties prop = new Properties();
        prop.put("hibernate.format_sql", "true");
        prop.put("hibernate.show_sql", "true");
        prop.put("hibernate.dialect",
                "org.hibernate.dialect.H2Dialect");
        prop.put("hibernate.hbm2ddl.auto","update");
        return prop;
    }

    @Bean
    public SessionFactory sessionFactory() {
        LocalSessionFactoryBuilder builder =
                new LocalSessionFactoryBuilder(dataSource());
        builder.scanPackages("org.university.data.model")
                .addProperties(getHibernateProperties());
        return builder.buildSessionFactory();
    }

    @Bean
    public Aspects collegeAspects() {
        return new Aspects();
    }
}
