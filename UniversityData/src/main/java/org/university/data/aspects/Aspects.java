package org.university.data.aspects;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;

import java.util.List;

@Aspect
public class Aspects {

    private static final Logger log = Logger.getLogger(Aspects.class);

    @AfterReturning(value = "execution(* com.tan4i4ik.dao.*.*.insert(..))", returning = "o")
    public void insert(Object o) {
         log.info(o.getClass().getSimpleName()  + " was added successfully");
     }

    @AfterReturning(value = "execution(* com.tan4i4ik.dao.*.*.delete*(..))", returning = "o")
    public void delete(JoinPoint jp, Object o ) {
        log.info(o.getClass().getSimpleName()  +" was deleted successfully");
    }

    @AfterReturning(value = "execution(* com.tan4i4ik.dao.*.*.update*(..))", returning = "o")
    public void update(JoinPoint jp, Object o ) {
        log.info(o.getClass().getSimpleName() + " was updated successfully ");
    }

    @AfterReturning(value = "execution(* com.tan4i4ik.dao.*.*.get*(..))", returning = "list")
    public void lists(JoinPoint jp, List list) {
        System.out.println(list);
        int lastElementTable = jp.getTarget().getClass().getSimpleName().indexOf("D");
        log.info("List of "+ jp.getTarget().getClass().getSimpleName().substring(0, lastElementTable).toLowerCase() + "s was getting from the table");
    }
}
