package org.university.data.dao;

import org.university.data.model.Student;
import org.university.data.model.StudentTopic;
import org.university.data.model.Topic;

public interface StudentTopicDAO {
    Integer insert(Student student, Topic topic, Integer graded);
    StudentTopic deleteById(Integer id);
}
