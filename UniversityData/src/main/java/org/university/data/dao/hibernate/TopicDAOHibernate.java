package org.university.data.dao.hibernate;

import org.university.data.dao.TopicDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.university.data.model.Course;
import org.university.data.model.Topic;

import java.util.ArrayList;
import java.util.List;

@Component
@Transactional
public class TopicDAOHibernate implements TopicDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private static final String SELECT_ALL = "FROM Topic";

    @Override
    public Topic insert(String name, String description, Course course) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Topic topic = new Topic(name,description, course);
        Integer i = (Integer) session.save(topic);
        topic.setId(i);
        tx.commit();
        return topic;
    }

    @Override
    public Topic update(Topic topic) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(topic);
        tx.commit();
        session.close();
        return topic;
    }

    @Override
    public Topic deleteById(Integer id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Topic topic = session.load(Topic.class, id);
        session.delete(topic);
        tx.commit();
        return topic;
    }

    @Override
    public List<Topic> getList() {
        Session session = sessionFactory.openSession();
        List<Topic> topics = session.createQuery(SELECT_ALL).list();
        if (topics.isEmpty()) {
            return new ArrayList<>();
        }
        return topics;
    }

    @Override
    public Topic get(Integer id) {
        Session session = sessionFactory.openSession();
        return session.get(Topic.class, id);
    }

}
