package org.university.data.dao.hibernate;

import org.university.data.dao.CollegeDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.university.data.model.College;

import java.util.*;

@Component
@Transactional
public class CollegeDAOHibernate implements CollegeDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private static final String SELECT_ALL = "FROM College";

    @Override
    public College insert(String nameCollege, String description) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        College college = new College(nameCollege,description);
        Integer i = (Integer) session.save(college);
        college.setId(i);
        tx.commit();
        session.close();
        return college;
    }

    @Override
    public College update(College college) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(college);
        tx.commit();
        session.close();
        return college;
    }

    @Override
    public College deleteById(Integer id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        College college = session.load(College.class, id);
        session.delete(college);
        tx.commit();
        return college;
    }

    @Override
    public List<College> getList() {
        Session session = sessionFactory.openSession();
        List<College> colleges = session.createQuery(SELECT_ALL).list();
        if (colleges.isEmpty())
            return new ArrayList<>();
        return colleges;
    }

    @Override
    public College get(Integer id) {
        Session session = sessionFactory.openSession();
        return session.get(College.class, id);
    }

    @Override
    public Map<Integer, String> nameColleges() {
        Session session = sessionFactory.openSession();
        List<College> colleges = session.createQuery(SELECT_ALL).list();
        Map<Integer, String> result = new HashMap<>();
        for (College college: colleges) {
            result.put(college.getId(), college.getName());
        }
        return result;
    }

}
