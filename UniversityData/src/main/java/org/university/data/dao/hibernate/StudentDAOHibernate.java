package org.university.data.dao.hibernate;

import org.university.data.dao.StudentDAO;
import org.university.data.model.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Component
@Transactional
public class StudentDAOHibernate implements StudentDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private static final String SELECT_ALL = "FROM Student";
    private static final String SELECT_BY_NAME = "FROM Student WHERE name = :name";
    private static final String SEARCH_BY_PART_NAME = "FROM Student WHERE name LIKE CONCAT('%', :name, '%')";

    @Override
    public Student insert(String name) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Student student = new Student(name);
        Integer i = (Integer) session.save(student);
        student.setId(i);
        tx.commit();
        return student;
    }

    @Override
    public Student update(Student student) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(student);
        tx.commit();
        session.close();
        return student;
    }

    @Override
    public Student deleteById(Integer id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Student student = session.load(Student.class, id);
        session.delete(student);
        tx.commit();
        return student;
    }

    @Override
    public List<Student> getList() {
        Session session = sessionFactory.openSession();
        List<Student> students = session.createQuery(SELECT_ALL).list();
        if(students.isEmpty())
            return new ArrayList<>();
        return students;
    }

    @Override
    public Student get(Integer id) {
        Session session = sessionFactory.openSession();
        return session.get(Student.class, id);
    }

    @Override
    public Student getByName(String name) {
        List<Student> studentList;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            studentList = session.createQuery(SELECT_BY_NAME).setParameter("name", name).list();
        } finally {
            session.close();
        }
        if(studentList.isEmpty())
            return null;
        return studentList.get(0);
    }

    @Override
    public List<Student> getByStudentName(String name) {
        Session session = sessionFactory.openSession();
        List<Student> list = session.createQuery(SEARCH_BY_PART_NAME).setParameter("name", name).list();
        if(list.isEmpty()) {
            return null;
        }
        return list;
    }
}
