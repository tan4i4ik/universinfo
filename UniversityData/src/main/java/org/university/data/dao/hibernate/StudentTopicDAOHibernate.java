package org.university.data.dao.hibernate;

import org.university.data.dao.StudentTopicDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.university.data.model.Student;
import org.university.data.model.StudentTopic;
import org.university.data.model.Topic;

@Component
@Transactional
public class StudentTopicDAOHibernate implements StudentTopicDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Integer insert(Student student, Topic topic, Integer graded) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        StudentTopic studentTopic = new StudentTopic(student,topic,graded);
        Integer id = (Integer) session.save(studentTopic);
        tx.commit();
        return id;
    }

    @Override
    public StudentTopic deleteById(Integer id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        StudentTopic studentTopic = session.load(StudentTopic.class, id);
        session.delete(studentTopic);
        tx.commit();
        return studentTopic;
    }
}
