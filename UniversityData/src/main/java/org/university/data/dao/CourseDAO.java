package org.university.data.dao;

import org.university.data.model.Course;
import org.university.data.model.Teacher;
import java.util.List;

public interface CourseDAO {
    Course insert(String name, String description, Teacher teacher);
    Course update(Course course);
    Course deleteById(Integer id);
    List<Course> getList();
    Course get(Integer id);
    List<Course> getCoursesByTeacherId(Integer id);
}
