package org.university.data.dao.hibernate;

import org.university.data.dao.CourseDAO;
import org.university.data.model.Course;
import org.university.data.model.Teacher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Component
@Transactional
public class CourseDAOHibernate implements CourseDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private static final String SELECT_COURSE_BY_TEACHER = "FROM Course WHERE teacher_id = :id";
    private static final String SELECT_ALL = "FROM Course";

    @Override
    public Course insert(String name, String description, Teacher teacher) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Course course = new Course(name,description,teacher);
        Integer i = (Integer) session.save(course);
        course.setId(i);
        tx.commit();
        return course;
    }

    @Override
    public Course update(Course course) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(course);
        tx.commit();
        session.close();
        return course;
    }

    @Override
    public Course deleteById(Integer id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Course course = session.load(Course.class, id);
        session.delete(course);
        tx.commit();
        return course;
    }

    @Override
    public List<Course> getList() {
        Session session = sessionFactory.openSession();
        List<Course> courses = session.createQuery(SELECT_ALL).list();
        if (courses.isEmpty())
            return new ArrayList<>();
        return courses;
    }

    @Override
    public Course get(Integer id) {
        Session session = sessionFactory.openSession();
        return session.get(Course.class, id);
    }

    @Override
    public List<Course> getCoursesByTeacherId(Integer id) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(SELECT_COURSE_BY_TEACHER);
        query.setParameter("id", id);
        return  query.getResultList();
    }

}
