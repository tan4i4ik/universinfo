package org.university.data.dao;

import org.university.data.model.College;

import java.util.List;
import java.util.Map;

public interface CollegeDAO {
    College insert(String nameCollege, String description);
    College update (College college);
    College deleteById(Integer id);
    List<College> getList();
    College get(Integer id);
    Map<Integer,String> nameColleges();
}
