package org.university.data.dao;

import org.university.data.model.Course;
import org.university.data.model.Topic;
import java.util.List;

public interface TopicDAO {
    Topic insert(String name, String description, Course course);
    Topic update(Topic topic);
    Topic deleteById(Integer id);
    List<Topic> getList();
    Topic get(Integer id);
}
