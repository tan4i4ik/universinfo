package org.university.data.dao;

import org.university.data.model.User;

public interface UserDAO {
    User insert(String login, String fullName, String email, String password);
    User getUserByLogin(String login);
}
