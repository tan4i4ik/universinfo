package org.university.data.dao;

import org.university.data.model.Student;
import java.util.List;

public interface StudentDAO {
    Student insert(String name);
    Student update(Student student);
    Student deleteById(Integer id);
    List<Student> getList();
    Student get(Integer id);
    Student getByName(String name);
    List<Student> getByStudentName(String name);
}
