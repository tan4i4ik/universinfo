package org.university.data.dao;

import org.university.data.model.College;
import org.university.data.model.Teacher;

import java.util.List;
import java.util.Map;

public interface TeacherDAO {
    Teacher insert(String name, String description, College college);
    Teacher update(Teacher teacher);
    Teacher deleteById(Integer id);
    List<Teacher> getList();
    Teacher get(Integer id);
    Map<Integer,String> nameTeachers();
    List<Teacher> getTeachersByCollegeId(Integer id);
}
