package org.university.data.dao.hibernate;

import org.university.data.dao.TeacherDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.university.data.model.College;
import org.university.data.model.Teacher;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Transactional
public class TeacherDAOHibernate implements TeacherDAO{

    @Autowired
    private SessionFactory sessionFactory;

    private static final String SELECT_TEACHER_BY_COLLEGE = "FROM Teacher WHERE college_id = :id";
    private static final String SELECT_ALL = "FROM Teacher";

    @Override
    public Teacher insert(String name, String description, College college) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Teacher teacher = new Teacher(name, description, college);
        Integer i = (Integer) session.save(teacher);
        tx.commit();
        return teacher;
    }

    @Override
    public Teacher update(Teacher teacher) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(teacher);
        tx.commit();
        session.close();
        return teacher;
    }

    @Override
    public Teacher deleteById(Integer id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Teacher teacher = session.load(Teacher.class, id);
        session.delete(teacher);
        tx.commit();
        return teacher;
    }

    @Override
    public List<Teacher> getList() {
        Session session = sessionFactory.openSession();
        List<Teacher> teachers = session.createQuery(SELECT_ALL).list();
        if(teachers.isEmpty()) {
            return new ArrayList<>();
        }
        return teachers;
    }

    @Override
    public List<Teacher> getTeachersByCollegeId(Integer id) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(SELECT_TEACHER_BY_COLLEGE);
        query.setParameter("id", id);
        return  query.getResultList();
    }

    @Override
    public Teacher get(Integer id) {
        Session session = sessionFactory.openSession();
        return session.get(Teacher.class, id);
    }

    @Override
    public Map<Integer, String> nameTeachers() {
        Session session = sessionFactory.openSession();
        List<Teacher> teachers = session.createQuery(SELECT_ALL).list();
        Map<Integer, String> result = new HashMap<>();
        for (Teacher teacher: teachers) {
            result.put(teacher.getId(), teacher.getName());
        }
        return result;
    }
}
