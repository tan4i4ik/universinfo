package org.university.data.dao.hibernate;

import org.university.data.dao.UserDAO;
import org.university.data.model.User;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
public class UserDAOHibernate implements UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private final String SELECT_USER = "FROM User WHERE username = :username";

    @Override
    public User insert(String login, String fullName, String email, String password) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        String str;
        User user;
        try {
            user = new User(login, fullName, email, password);
            session.save(user);
            tx.commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
            user = null;
        }
        return user;
    }

    @Override
    @SuppressWarnings("unchecked")
    public User getUserByLogin(String login) {
        Session session = sessionFactory.openSession();
        User user = (User) session.createQuery(SELECT_USER).setParameter("username", login).uniqueResult();
        return user;
    }
}
