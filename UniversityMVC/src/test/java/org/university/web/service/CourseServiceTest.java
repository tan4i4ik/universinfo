package org.university.web.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.university.data.dao.StudentDAO;
import org.university.data.dao.StudentTopicDAO;
import org.university.data.dao.TopicDAO;
import org.university.data.model.Student;
import org.university.data.model.Topic;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class CourseServiceTest {

    @Mock
    StudentDAO studentDao;

    @Mock
    TopicDAO topicDao;

    @Mock
    StudentTopicDAO studentTopicDao;

    @InjectMocks
    CourseServiceImpl courseService;

    @Before
    public void initialize() throws Exception{
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addStudent() {
        Student student = new Student();
        Topic topic = new Topic();
        when(studentDao.getByName("Jack Tomson")).thenReturn(null);
        when(studentDao.getByName("Mike Tomson")).thenReturn(student);
        when(topicDao.get(1)).thenReturn(topic);
        when(studentDao.insert("Jack Tomson")).thenReturn(student);
        when(studentTopicDao.insert(student,topic,4)).thenReturn(4);

        assertEquals((long) 4, (long) courseService.addStudentTopic("Jack Tomson", 1, 4));
        assertEquals((long) 4, (long) courseService.addStudentTopic("Mike Tomson", 1, 4));

    }
}
