package org.university.web.service.interfaces;

import org.university.data.model.Course;

import java.util.List;
import java.util.Map;

public interface CourseService {
    List<Course> getList();
    void add(String name, String description, Integer idTeacher);
    void delete(int i);
    void edit(int i, String name, String description, Integer idTeacher);
    String teacherNameById(Integer id);
    Course get(int id);
    Map<Integer,String> getTeachers();
    void deleteGraded(Integer id);
    Integer addStudentTopic(String studentName, Integer idTopic, Integer graded);
}
