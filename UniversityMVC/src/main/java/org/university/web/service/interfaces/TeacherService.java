package org.university.web.service.interfaces;

import org.university.data.model.Course;
import org.university.data.model.Teacher;

import java.util.List;
import java.util.Map;

public interface TeacherService
{
    List<Teacher> getList();
    Teacher add(String name, String description, Integer idCollege);
    void delete(int i);
    void edit(int i, String name, String description, Integer idCollege);
    List<Course> subjects(int i);
    Teacher get(int id);
    Map<Integer, String> getColleges();
    String collegeNameById(Integer id);
}
