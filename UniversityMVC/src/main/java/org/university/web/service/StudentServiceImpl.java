package org.university.web.service;

import org.university.data.dao.StudentDAO;
import org.university.data.dao.StudentTopicDAO;
import org.university.data.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.university.web.service.interfaces.StudentService;

import java.util.List;

@Service("studentService")
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDAO studentDao;

    @Autowired
    private StudentTopicDAO studentTopicDao;

    @Override
    public List<Student> getList() {
        return studentDao.getList();
    }

    @Override
    public void edit(int i, Student student) {
        student.setId(i);
        studentDao.update(student);
    }

    @Override
    public void delete(int i) {
         studentDao.deleteById(i);
    }

    @Override
    public void add(Student student) {
        studentDao.insert(student.getName());
    }

    @Override
    public Student get(int id) {
        return studentDao.get(id);
    }

    @Override
    public List<Student> getByName(String name) {
        return studentDao.getByStudentName(name);
    }

    @Override
    public void deleteGraded(Integer id) {
        studentTopicDao.deleteById(id);
    }
}
