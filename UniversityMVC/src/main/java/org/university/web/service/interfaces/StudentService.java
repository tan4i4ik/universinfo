package org.university.web.service.interfaces;

import org.university.data.model.Student;

import java.util.List;

public interface StudentService {
    List<Student> getList();
    void edit(int i, Student student);
    void delete(int i);
    void add(Student student);
    Student get(int id);
    List<Student> getByName(String name);
    void deleteGraded(Integer id);
}
