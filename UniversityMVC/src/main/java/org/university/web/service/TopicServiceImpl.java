package org.university.web.service;

import org.university.data.dao.CourseDAO;
import org.university.data.dao.TopicDAO;
import org.university.data.model.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.university.web.service.interfaces.TopicService;

@Service("topicService")
public class TopicServiceImpl implements TopicService {

    @Autowired
    private TopicDAO topicDAO;

    @Autowired
    private CourseDAO courseDAO;

    @Override
    public void add(String name, String description, int i) {
        topicDAO.insert(name,description, courseDAO.get(i));
    }

    @Override
    public void delete(Integer id) {
        topicDAO.deleteById(id);
    }

    @Override
    public void edit(int idTopic, String name, String description) {
        topicDAO.update(new Topic(idTopic,name,description, topicDAO.get(idTopic).getCourse()));
    }
}
