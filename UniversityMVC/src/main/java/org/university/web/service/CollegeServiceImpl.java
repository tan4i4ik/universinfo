package org.university.web.service;

import org.university.data.dao.CollegeDAO;
import org.university.data.dao.TeacherDAO;
import org.university.data.model.College;
import org.university.data.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.university.web.service.interfaces.CollegeService;

import java.util.List;

@Service("collegeService")
public class CollegeServiceImpl implements CollegeService {
    @Autowired
    private CollegeDAO collegeDao;

    @Autowired
    private TeacherDAO teacherDao;

    @Override
    public List<College> getList(){
        return collegeDao.getList();
    }

    @Override
    public void add(College college) {
        collegeDao.insert(college.getName(), college.getDescription());
    }

    @Override
    public void delete(Integer id) {
        collegeDao.deleteById(id);
    }

    @Override
    public void edit(College college) {
        collegeDao.update(college);
    }

    @Override
    public List<Teacher> teachers(int i) {
        return teacherDao.getTeachersByCollegeId(i);
    }

    @Override
    public College get(int id) {
        return collegeDao.get(id);
    }
}
