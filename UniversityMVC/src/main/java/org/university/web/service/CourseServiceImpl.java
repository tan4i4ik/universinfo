package org.university.web.service;

import org.university.data.dao.*;
import org.university.data.model.Course;
import org.university.data.model.Student;
import org.university.data.model.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.university.web.service.interfaces.CourseService;

import java.util.List;
import java.util.Map;

@Service("courseService")
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseDAO courseDAO;

    @Autowired
    private TeacherDAO teacherDAO;

    @Autowired
    private TopicDAO topicDAO;

    @Autowired
    private StudentTopicDAO studentTopicDAO;

    @Autowired
    private StudentDAO studentDao;

    @Override
    public List<Course> getList() {
        return courseDAO.getList();
    }

    @Override
    public void edit(int i, String name, String description, Integer idTeacher) {
        courseDAO.update(new Course(i, name, description, teacherDAO.get(idTeacher)));
    }

    @Override
    public void delete(int i) {
        courseDAO.deleteById(i);
    }

    @Override
    public void add(String name, String description, Integer idTeacher) {
        courseDAO.insert(name,description,teacherDAO.get(idTeacher));
    }

    @Override
    public Course get(int id) {
        return courseDAO.get(id);
    }

    @Override
    public String teacherNameById(Integer id) {
        return courseDAO.getList().get(id).getName();
    }

    @Override
    public Map<Integer,String> getTeachers() {
        return teacherDAO.nameTeachers();
    }

    @Override
    public void deleteGraded(Integer id) {
        studentTopicDAO.deleteById(id);
    }

    @Override
    public Integer addStudentTopic(String studentName, Integer idTopic, Integer graded) {
        Topic topic = topicDAO.get(idTopic);
        Student student = studentDao.getByName(studentName);
        if( student == null) {
            student = studentDao.insert(studentName);
        }
        return studentTopicDAO.insert(student,topic,graded);
    }
}
