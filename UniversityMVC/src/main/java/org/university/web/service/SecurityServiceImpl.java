package org.university.web.service;

import org.university.data.dao.UserDAO;
import org.university.web.security.UserDetailsInfo;
import org.university.data.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.university.web.service.interfaces.SecurityService;

@Service("securityService")
public class SecurityServiceImpl implements SecurityService {

    @Autowired
    private UserDAO dao;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = dao.getUserByLogin(username);
        if (user == null){
            throw new UsernameNotFoundException("no such username!");
        }
        return new UserDetailsInfo(user);
    }
}
