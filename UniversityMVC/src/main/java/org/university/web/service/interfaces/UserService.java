package org.university.web.service.interfaces;

import org.university.data.model.User;

public interface UserService {
    User save(User account);
    User findByLogin(String username);
}
