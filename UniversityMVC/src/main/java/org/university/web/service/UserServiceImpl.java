package org.university.web.service;

import org.university.data.dao.UserDAO;
import org.university.data.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.university.web.service.interfaces.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO dao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public User save(User account) {
        return dao.insert(account.getUsername(), account.getFullName(), account.getEmail(), bCryptPasswordEncoder.encode(account.getPassword()));
    }

    public User findByLogin(String username) {
        return dao.getUserByLogin(username);
    }
}


