package org.university.web.service;

import org.university.data.dao.CollegeDAO;
import org.university.data.dao.CourseDAO;
import org.university.data.dao.TeacherDAO;
import org.university.data.model.College;
import org.university.data.model.Course;
import org.university.data.model.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.university.web.service.interfaces.TeacherService;

import java.util.List;
import java.util.Map;

@Service("teacherService")
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    private TeacherDAO teacherDao;

    @Autowired
    private CollegeDAO collegeDao;

    @Autowired
    private CourseDAO courseDao;

    @Override
    public List<Teacher> getList(){
        return teacherDao.getList();
    }

    @Override
    public Teacher add(String name, String description, Integer idCollege) {
        return teacherDao.insert(name, description, collegeDao.get(idCollege));
    }

    @Override
    public void delete(int i) {
        teacherDao.deleteById(i);
    }

    @Override
    public void edit(int i, String name, String description, Integer idCollege) {
        teacherDao.update(new Teacher(i, name,description, collegeDao.get(idCollege)));
    }

    @Override
    public List<Course> subjects(int i) {
        return courseDao.getCoursesByTeacherId(i);
    }

    @Override
    public Teacher get(int id) {
        return teacherDao.get(id);
    }

    @Override
    public Map<Integer, String> getColleges() {
        return collegeDao.nameColleges();
    }

    @Override
    public String collegeNameById(Integer id) {
        return teacherDao.getList().get(id).getCollege().getName();
    }
}
