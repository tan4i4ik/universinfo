package org.university.web.service.interfaces;

import org.university.data.model.College;
import org.university.data.model.Teacher;

import java.util.List;

public interface CollegeService {
    List<College> getList();
    void add(College college);
    void delete(Integer id);
    void edit(College college);
    List<Teacher> teachers(int i);
    College get(int id);
}
