package org.university.web.service.interfaces;

public interface TopicService {
    void add(String name, String description, int i);
    void delete(Integer id);
    void edit(int idTopic, String name, String description);
}
