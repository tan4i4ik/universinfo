package org.university.web.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.university.data.model.Course;
import org.university.data.model.Teacher;
import org.university.web.service.interfaces.TeacherService;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller("teacherController")
@RequestMapping("teachers")
public class TeacherController {

    private static final Logger log = Logger.getLogger(TeacherController.class);

    @Autowired
    private TeacherService teacherService;

    @GetMapping
    public String view(Model model) throws IOException {
        log.info("Redirect to teachers");
        return "teachers";
    }

    @GetMapping("/list")
    @ResponseBody
    public List<Teacher> list(){
        List<Teacher> list = teacherService.getList();
        log.info("Get list of teachers. Size of list " + list.size());
        return list;
    }

    @PostMapping("/add")
    @ResponseBody
    public Teacher add(@RequestParam String name, @RequestParam String description, @RequestParam String idCollege) {
        log.info("Add " + name + " to teachers");
        return teacherService.add(name, description, Integer.parseInt(idCollege));
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public void delete(@PathVariable("id") String id) {
        log.info("Delete teacher by id = " + id);
        teacherService.delete(Integer.parseInt(id));
    }

    @PostMapping("/edit/{id}")
    @ResponseBody
    public void edit(@PathVariable("id") String id, @RequestParam String name, @RequestParam String description, @RequestParam String idCollege) {
        log.info("Edit teacher by id = " + id);
        teacherService.edit(Integer.parseInt(id),name, description, Integer.parseInt(idCollege));
    }

    @GetMapping("/subjects{id}")
    @ResponseBody
    public List<Course> courses(@PathVariable("id") String id) {
        List<Course> list = teacherService.subjects(Integer.parseInt(id));
        log.info("Edit list of subjects of teacher with id =  " + id);
        return list;
    }

    @GetMapping("/colleges")
    @ResponseBody
    public Map<Integer,String> colleges(){
        Map<Integer,String> map = teacherService.getColleges();
        log.info("Get list of colleges.Size of list " + map.size());
        return map;
    }

    @GetMapping("/teacher{id}/collegeId")
    @ResponseBody
    public String collegeById(@PathVariable("id")String id){
        log.info("Get collegeId for teacherId = " + id);
        return teacherService.collegeNameById(Integer.parseInt(id));
    }
}
