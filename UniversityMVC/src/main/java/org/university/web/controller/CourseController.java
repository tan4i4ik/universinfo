package org.university.web.controller;

import org.university.data.model.Course;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.university.web.service.interfaces.CourseService;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller("courseController")
@RequestMapping("courses")
public class CourseController {

    private static final Logger log = Logger.getLogger(CourseController.class);

    @Autowired
    private CourseService courseService;

    @GetMapping
    public String view() throws IOException {
        log.info("Redirect to courses");
        return "courses";
    }

    @GetMapping("/list")
    @ResponseBody
    public List<Course> list() {
        List<Course> list = courseService.getList();
        log.info("Get list of courses. Size of list " + list.size());
        return list;
    }

    @PostMapping("/add")
    @ResponseBody
    public void add(@RequestParam String name, @RequestParam String description, @RequestParam String idTeacher) {
        log.info("Add " + name + " to courses");
        courseService.add(name, description, Integer.parseInt(idTeacher));
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public void delete(@PathVariable("id") String id) {
        log.info("Delete courses by id = " + id);
        courseService.delete(Integer.parseInt(id));
    }

    @PostMapping("/edit/{id}")
    @ResponseBody
    public void edit(@PathVariable("id") String id, @RequestParam String name, @RequestParam String description, @RequestParam String idTeacher) {
        log.info("Edit college " + name);
        courseService.edit(Integer.parseInt(id),name,description, Integer.parseInt(idTeacher));
    }

    @GetMapping("/teachers")
    @ResponseBody
    public Map<Integer,String> teachers(){
        log.info("Get teachers list");
        Map<Integer, String> teachers = courseService.getTeachers();
        return teachers;
    }

    @DeleteMapping("/delete/graded/{graded_id}")
    @ResponseBody
    public void deleteGraded( @PathVariable("graded_id") String idGraded) {
        log.info("Delete student grade by id =" + idGraded);
        courseService.deleteGraded(Integer.parseInt(idGraded));
    }

    @PostMapping(value = "add/topic/{id_topic}/add_student")
    @ResponseBody
    public Integer addStudentTopic( @PathVariable("id_topic") String idTopic, @RequestParam String name,  @RequestParam String grade) {
        log.info("Add student: " + name + " with grade " + grade);
        return courseService.addStudentTopic(name, Integer.parseInt(idTopic), Integer.parseInt(grade));
    }
}
