package org.university.web.controller;

import org.university.data.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.university.web.service.interfaces.UserService;

@Controller("mainController")
public class MainController {

    private static final Logger log = Logger.getLogger(MainController.class);

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String mainPage(Model model) {
        log.info("Redirect to login");
        return "login";
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        log.info("Redirect to registration");
        return "registration";
    }

    @PostMapping("/registration")
    public ModelAndView registration(@ModelAttribute User user) {
        String password = user.getPassword();
        User savedUser = userService.save(user);
        if (savedUser != null ) {
            log.info("User successfully added");
            log.info("Redirect to login");
            return new ModelAndView("login");
        } else {
            ModelAndView model = new ModelAndView();
            log.info("User was created before");
            model.addObject("result", "The account was created before.");
            model.setViewName("registration");
            return model;
        }
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout){
        if (error != null){
            log.info("Your login and password is invalid");
            model.addAttribute("error", "Your login and password is invalid");
        }

        if (logout != null){
            log.info("You have been logged out successfully");
            model.addAttribute("message", "You have been logged out successfully");
        }
        return "login";
    }

    @GetMapping("/main")
    public String main(Model model) {
        log.info("Redirect to main");
        return "main";
    }

    @GetMapping("/check")
    public String check(Model model) {
        log.info("Redirect to main");
        return "check";
    }

}
