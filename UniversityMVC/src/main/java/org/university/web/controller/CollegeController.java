package org.university.web.controller;

import org.university.data.model.College;
import org.university.data.model.Teacher;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.university.web.service.interfaces.CollegeService;

import java.io.IOException;
import java.util.List;

@Controller("collegeController")
@RequestMapping("colleges")
public class CollegeController {

    private static final Logger log = Logger.getLogger(CollegeController.class);

    @Autowired
    private CollegeService collegeService;

    @GetMapping
    public String view(Model model) throws IOException {
        log.info("Redirect to colleges");
        return "colleges";
    }

    @GetMapping("/list")
    @ResponseBody
    public List<College> list(){
        List<College> list = collegeService.getList();
        log.info("Get list of colleges. Size of list " + list.size());
        return list;
    }

    @PostMapping("/add")
    public  @ResponseBody void add(@RequestBody College college) {
        log.info("Add " + college.getName() + " to colleges");
        collegeService.add(college);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public void delete(@PathVariable("id") String id) {
        log.info("Delete college by id = " + id);
        collegeService.delete(Integer.parseInt(id));
    }

    @PostMapping("/edit/{id}")
    public @ResponseBody void edit(@PathVariable("id") String id, @RequestBody College college) {
        log.info("Edit college by id = " + id);
        college.setId(Integer.parseInt(id));
        collegeService.edit(college);
    }

    @GetMapping("/teachers{id}")
    @ResponseBody
    public List<Teacher> teachers(@PathVariable("id") String id) {
        log.info("Get teachers list");
        return collegeService.teachers(Integer.parseInt(id));
    }
}
