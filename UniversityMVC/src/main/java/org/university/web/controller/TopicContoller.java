package org.university.web.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.university.web.service.interfaces.TopicService;

@Controller("topicController")
@RequestMapping("topics")
public class TopicContoller {

    private static final Logger log = Logger.getLogger(TopicContoller.class);

    @Autowired
    private TopicService topicService;

    @PostMapping("/add")
    @ResponseBody
    public void add(@RequestParam String name, @RequestParam String description, @RequestParam String id) {
        log.info("Add " + name + " to topics");
        topicService.add(name, description, Integer.parseInt(id));
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public void delete(@PathVariable("id") String id) {
        log.info("Delete topic by id = " + id);
        topicService.delete(Integer.parseInt(id));
    }

    @PostMapping("/edit/{id}")
    @ResponseBody
    public void edit(@PathVariable("id") String id, @RequestParam String name, @RequestParam String description) {
        log.info("Edit topic by id = " + id);
        topicService.edit(Integer.parseInt(id), name, description);
    }
}
