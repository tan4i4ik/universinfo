package org.university.web.controller;

import org.university.data.model.Student;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.university.web.service.interfaces.StudentService;

import java.io.IOException;
import java.util.List;

@Controller("studentController")
@RequestMapping(value = "students")
public class StudentController {

    private static final Logger log = Logger.getLogger(StudentController.class);

    @Autowired
    private StudentService studentService;

    @GetMapping
    public String view(Model model) throws IOException {
        log.info("Redirect to students");
        return "students";
    }

    @GetMapping("/list")
    @ResponseBody
    public List<Student> list(){
        List<Student> list = studentService.getList();
        log.info("Get list of students. Size of list " + list.size());
        return list;
    }

    @PostMapping("/add")
    @ResponseBody
    public void add(@RequestBody Student student) {
        log.info("Add " + student.getName() + " to students");
        studentService.add(student);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") String id) {
        log.info("Delete student by id = " + id);
        studentService.delete(Integer.parseInt(id));
        return id;
    }

    @PostMapping("/edit/{id}")
    @ResponseBody
    public void edit(@PathVariable("id") String id, @RequestBody Student student) {
        log.info("Edit student by id = " + id);
        studentService.edit(Integer.parseInt(id),student);
    }

    @GetMapping("/get")
    @ResponseBody
    public List<Student> get( @RequestParam String name){
        log.info("Get students with name " + name);
        List<Student> list = studentService.getByName(name);
        return list;
    }


    @DeleteMapping("/delete/graded/{graded_id}")
    @ResponseBody
    public void deleteGraded( @PathVariable("graded_id") String idGraded) {
        log.info("Delete student grade with id =" + idGraded);
        studentService.deleteGraded(Integer.parseInt(idGraded));
    }
}
