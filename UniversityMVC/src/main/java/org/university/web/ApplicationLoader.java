package org.university.web;

import org.apache.log4j.Logger;
import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.university.data.configuration.ProjectConfig;
import org.university.web.config.AppConfig;
import org.university.web.config.SecurityConfiguration;

import java.sql.SQLException;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = {"org.university.web"})
@EntityScan("org.university.data.model")
@Import({AppConfig.class, ProjectConfig.class, SecurityConfiguration.class})
public class ApplicationLoader{

    private static final Logger logger = Logger.getLogger(ApplicationLoader.class);

    public static void main(String[] args) throws Exception{
        logger.info("Initialize application");
        SpringApplication.run(ApplicationLoader.class);
    }

    @Value("${h2.tcp.port:9092}")
    private String h2TcpPort;

    @Value("${h2.web.port:8082}")
    private String h2WebPort;

    @Bean
    @ConditionalOnExpression("${h2.tcp.enabled:true}")
    public Server h2TcpServer() throws SQLException {
        return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", h2TcpPort).start();
    }

    /****get access to H2 database web (path to db - jdbc:h2:mem:db )**********/
    @Bean
    @ConditionalOnExpression("${h2.web.enabled:true}")
    public Server h2WebServer() throws SQLException {
        return Server.createWebServer("-web", "-webAllowOthers", "-webPort", h2WebPort).start();
    }
}
