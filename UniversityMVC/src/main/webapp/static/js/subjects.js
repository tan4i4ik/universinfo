$(document).ready(function(){
  var teacherList;
    /*******Add subjects to scrollspy*********/
  function addScrollSpy(data) {
    $(".list_subjects").append("<a href='#subject"+ data.id + "' class='name" + data.id +" list-group-item list-group-item-action' ><b>" +data.name+ "</b></a>");
    $("#scrollspy-list").append("<div id = 'subject" + data.id + "' class='mb-2'/>");

    $("#subject" + data.id).append("<h2 class = 'name'>"+data.name +"</h2>");
    $("#subject" + data.id).append("<p class='teacher" + data.id + "' style='text-align: justify;'><i>" + "Professor's name: " + "<a href = 'teachers#teacher" + data.teacher_id +"'>" + data.teacher_name + "</a></i></p>");
    $("#subject" + data.id).append("<p class='description" + data.id + "' style='text-align: justify;'>" + data.description + "</p>");
    $("#subject" + data.id).append("<div id='topicList" + data.id + "' class='list-group'/>");
    $("#subject" + data.id).append("<button id='addTopic" +data.id + "' type='button' class='btn btn-info mt-2 addTopic' data-toggle='modal' data-target='#addModalTopic" +data.id + "'>New Topic</button>");
    addTopicModal(data.id);
    $("#subject" + data.id).append("<hr class='my-4'/>");

    $.each(data.topics, function(){
      $("#subject" + data.id + " #topicList" + data.id).append("<button id='topic" + data.id + "' class='list-group-item list-group-item-action' data-toggle='modal' data-target='#modalTopic" + this.id +  "'>" +this.name + "</button>");
      openTopicModal(data.id, this);  
    });

  }

  function teacherSelect(select) {
    $.map(teacherList, function(value, key) {
      $(select).append('<label for="teachers">Select teacher :</label><option id=teacher'+key+'>' + value + '</option>');
    });
  }

  function printTable(id,data) { 
    var i = 1;
    $(id).append("<table class='table table-bordered students' name ='" + id + "'/>");
    $(id + " table").append("<thead></thead>");
    $(id + " table thead").append("<tr></tr>");
    $(id + " table thead tr").append("<th scope='col'>#</th>");
    $(id + " table thead tr").append("<th scope='col'>Name </th>");
    $(id + " table thead tr").append("<th scope='col'>Graded</th>");
    $(id + " table thead tr").append("<th scope='col'>Delete</th>");
    $(id + " table").append("<tbody></tbody");
      $.each(data, function(){
         $(id + " table tbody").append("<tr class='row" + i + "'></tr>");
         $(id + " table tbody .row" + i).append("<th scope = 'row'>" + i + "</th>");
         $(id + " table tbody .row" + i).append("<td>" + this.name + "</td>");
         $(id + " table tbody .row" + i).append("<td>" + this.graded + "</td>");
         $(id + " table tbody .row" + i).append("<td><button type='button' class='btn btn-info ml-2 delete_grade' name='"+this.id+"' >Delete</button></td>");
         i++;
      });
  }

  function openTopicModal(id,data) {
    var flag = false; 
    $("body").append('<div class="modal fade " id="modalTopic' + data.id + '" tabindex="-1" role="dialog" aria-labelledby="topicLabel" aria-hidden="true"/>');
    $("body #modalTopic" + data.id).append('<div class="modal-dialog modal-lg" role="document"/>');
    $("body #modalTopic" + data.id + " .modal-dialog").append('<div class="modal-content"/>');

    /*****************Add content to modal*************************/
    $("body #modalTopic" + data.id + " .modal-content").append('<div class="modal-header"/>');
    $("body #modalTopic" + data.id + " .modal-content").append('<div class="modal-body"/>');
    $("body #modalTopic" + data.id + " .modal-content").append('<div class="modal-footer"/>');

    /******************Add modal-header info*************************/
    $("body #modalTopic" + data.id + " .modal-header").append('<h5 class="modal-title" id="topicLabel">' + data.name + '</h5>');
    $("body #modalTopic" + data.id + " .modal-header").append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"/>');
    $("body #modalTopic" + data.id + " .modal-header button").append('<span aria-hidden="true">&times;</span>');
   
    /******************Add modal-body info*************************/   
    $("body #modalTopic" + data.id + " .modal-body").append('<form></form>');
    $("body #modalTopic" + data.id + " .modal-body form").append('<div class="modal-description form-group"/>');
    $("body #modalTopic" + data.id + " .modal-body .modal-description").append('<label for="addTopicName" class="form-control-label">' + data.description + '</label>');
    $("body #modalTopic" + data.id + " .modal-body form").append('<div class="modal-table form-group"/>');
    $("body #modalTopic" + data.id + " .modal-body .modal-table").append('<label for="tbl_student_list'+data.id + '" class="form-control-label">Student\'s Graded List</label>');
    printTable("body #modalTopic" + data.id + " .modal-body .modal-table", data.students);
    if(data.students.length == 0) {
      $("body #modalTopic" + data.id + " .modal-body .modal-table").hide();
    } 
    $("body #modalTopic" + data.id + " .modal-body form").append('<div id="add_student' + data.id + '"class="form-group"/>');

    /******************Add modal-footer info*************************/
    $("body #modalTopic" + data.id + " .modal-footer").append('<button id="btnAddStudent' +data.id +'" class = "btn btn-info add_student" name="' + data.id +'">Add student</button>');
    $("body #modalTopic" + data.id + " .modal-footer").append('<button type="button" class="btn btn-info ml-2" data-toggle="tooltip" data-placement="top" id="btnDeleteTopic' +data.id + '">Delete topic</button>');
    $("body #modalTopic" + data.id + " .modal-footer").append('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
   
    $("#btnDeleteTopic" + data.id).click(function(){
      ajaxSendObject("DELETE", "subjects/"+id + "/delete/topic/" + data.id, null,'html', success_result);
    });
    $(".delete_grade").click(function(){
      ajaxSendObject("DELETE", "subjects/"+id + "/delete/graded/" + this.name, null,'html', success_result);
    });

    $("#btnAddStudent"+data.id).click(function(){
      if (flag == false) {
        flag = true;
        var add_student = '<div class="form-group">' +
                            '<input type="text" class="form-control" id="add_student_name' + data.id + '" placeholder="Input student\'s name">' +
                          '</div>' +
                          '<div class="form-group">' +
                            '<input type="text" class="form-control" id="addStudentGrade' + data.id + '" placeholder="Input student\'s graded"/>' +
                          '</div>' + 
                          '<div class="form-group">' +
                          '<button type="submit" class="btn btn-primary submit_add_student"  >Submit</button>' +
                          '</div>';
        $("#add_student" + data.id).append(add_student);

        $(".submit_add_student").click(function(event){
          addData = {"name" : $("#add_student_name" + data.id).val(), "grade" : $("#addStudentGrade" + data.id).val()};
          var successAddStudent = function (id) {
            event.preventDefault();
            $("#add_student" + data.id).html('');
            if($("body #modalTopic" + data.id + " .modal-body .modal-table").is(":hidden")) {
              $("body #modalTopic" + data.id + " .modal-body .modal-table").show();
            }
           var countRow  = $("body #modalTopic" + data.id + " .modal-body table tbody tr").length + 1;
           $("body #modalTopic" + data.id + " .modal-body table tbody").append("<tr class='row" + countRow + "'></tr>");
           $("body #modalTopic" + data.id + " .modal-body table tbody .row" + countRow).append('<th scope = "row">' + countRow + '</th>');
           $("body #modalTopic" + data.id + " .modal-body table tbody .row" + countRow).append('<td>' + addData["name"] +  '</td>');
           $("body #modalTopic" + data.id + " .modal-body table tbody .row" + countRow).append('<td>' + addData["grade"] +  '</td>');
           $("body #modalTopic" + data.id + " .modal-body table tbody .row" + countRow).append('<td><button type="button" class="btn btn-info ml-2 delete_grade" name="'+id+'" >Delete</button></td>');


          } 
          ajaxSendObject("POST", "subjects/" + id + "/add/topic/" + data.id + "/add_student", addData,'html', successAddStudent);
        });
      } else {
        $("#add_student" + data.id).html('');
        flag = false;
      }

    });


    $("#modalTopic" + data.id).on('hidden.bs.modal', function () {
      $("#add_student" + data.id).html('');
    });

  }
  function addTopicModal(id){
    $("body").append("<div class='modal fade' id='addModalTopic" + id + "' tabindex='-1' role='dialog' aria-labelledby='addTopicLabel' aria-hidden='true'/>" );
    $("#addModalTopic" + id).append("<div class='modal-dialog' role='document'/>");
    $("#addModalTopic" + id + " .modal-dialog").append("<div class='modal-content'/>");

    /*****************Add content to modal*************************/
    $("#addModalTopic" + id + " .modal-content").append("<div class='modal-header'/>");
    $("#addModalTopic" + id + " .modal-content").append("<div class='modal-body'/>");
    $("#addModalTopic" + id + " .modal-content").append("<div class='modal-footer'/>");

    /******************Add modal-header info*************************/  
    $("#addModalTopic" + id + " .modal-header").append("<h5 class='modal-title' id='addTopicLabel'>Create new topic</h5>");
    $("#addModalTopic" + id + " .modal-header").append("<button type='button' class='close' data-dismiss='modal' aria-label='Close'/>");
    $("#addModalTopic" + id + " .modal-header button").append("<span aria-hidden='true'>&times;</span>");
   
    /******************Add modal-body info*************************/   
    $("#addModalTopic" + id + " .modal-body").append("<form></form>");
    $("#addModalTopic" + id + " .modal-body form").append("<div class='modalTopicName form-group'/>");
    $("#addModalTopic" + id + " .modalTopicName").append("<label for='addTopicName' class='form-control-label'>Topic name:</label>");
    $("#addModalTopic" + id + " .modalTopicName").append("<input type='text' class='form-control' id='addTopicName" + id + "'/>");
    $("#addModalTopic" + id + " .modal-body form").append("<div class='modalTopicDescribtion form-group'/>");
    $("#addModalTopic" + id + " .modalTopicDescribtion").append("<label for='addTopicDescription' class='form-control-label'>Description of the topic:</label>");
    $("#addModalTopic" + id + " .modalTopicDescribtion").append("<textarea class='form-control' id='addTopicDescription" + id +"'/>");
    
    /******************Add modal-footer info*************************/ 
    $("#addModalTopic" + id + " .modal-footer").append("<button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button>"); 
    $("#addModalTopic" + id + " .modal-footer").append("<button type='submit' class='btn btn-primary' id='addTopicSubmit" + id + "' data-dismiss='modal' class='submitTopic'>Submit</button>");  

     $("#addTopicSubmit" + id).click(function(){
      data = {"id": id, "name" : $("#addTopicName" + id).val(), "description":$("#addTopicDescription" + id).val()};
      ajaxSendObject("POST", "subjects/" + id +"/add_topic", data,'html', success_result);
    });
  }

  /*********Add delete button*************************/
  function addDeleteButton(id, path) {
    var button = $("<button type='button' class='btn btn-info ml-2' data-toggle='tooltip' data-placement='top' title='Delete subject' id='delete" + id+ "'>Delete</button>");
    ajaxSendObject("DELETE","subjects/delete/"+id,null,null,success_result);
    $(path).append(button);
  }
  
  /*********Add edit button*************************/
  function addEditButton(id, path) {
    var button = $("<button type='button' class='btn btn-info ml-2 edit' data-toggle='tooltip' data-placement='top' title='Edit subject' id='edit" + id+ "' data-toggle='modal' data-target='#editModal'>Edit</button>");
    $(button).click(function() {
      name = $(".name" + id).text();
      description = $(".description" + id).text();
      $('#editHeader').text("Edit information about " + name);
      $("#editName").attr("value", name);
      $("#editDescription").val(description);
      $('#selectEdit').append('<select class="form-control" id="teacherEdit" data-live-search="true"></select>');
      teacherSelect("#teacherEdit");
      $('#editModal').attr("name", id);
      $('#editModal').modal('show');
    });
    $(path).append(button);
  }

  $("body").ready(function(){
    $.ajax({
      type : "GET",
      url : "subjects/teachers",
      success : function(teachers) {
        teacherList = teachers;
        var successShowSubjects = function(data) {
          $.each(data, function() {
            var id = this.id;
            addScrollSpy(this);
            addEditButton(id, "#subject" + id + " h2");
            addDeleteButton(id, "#subject" + id + " h2");
          });
        };

        ajaxSendObject("GET", "subjects/list", null, 'json', successShowSubjects);
      }, 
      error : function(jqXHR, e) {
        console.log(jqXHR);
        console.log('Error: ' + e);
      }
    });
    console.log(teachers);


    $("#signOut").click(function(){
      window.location.replace("/university");
    });
  });

  $("#submit").click(function(){
    var teacher = $("#teachers option:selected").val();
      var idTeacher;

      $.map(teacherList, function(value,key){
          if(value == teacher) {
            idTeacher = key;
          }
      });
       var data = {"name": $('#addName').val(), "description" : $('#addDescription').val(), "idTeacher" : idTeacher };
       $('#select').empty();
       ajaxSendObject("POST","subjects/add", data, 'html', success_result);
  });


$("#addButton").click(function(){
  teacherSelect("#teachers");
});
  $("#submitEdit").click(function(){
      var id = $('#editModal').attr("name");
      var teacher = $("#teacherEdit option:selected").val();
      var idTeacher;

      $.map(teacherList, function(value,key){
        if(value == teacher) {
          idTeacher = key;
        }
      });
      $('#selectEdit').empty();
      var data = {"name" : $("#editName").val(), "description" : $('#editDescription').val(), "idTeacher" : idTeacher};
      ajaxSendObject("POST", "subjects/edit/"+ id, data, 'html', success_result);
  });


  function ajaxSendObject(type,url,data,dataType, func) {
    $.ajax({
            type : type,
            url : url,
            data: data,
            dataType:  dataType,
            success : function(id) {
              console.log(type + " was successful.");
              func(id, data);
            },
            error : function(jqXHR, e) {
                console.log(jqXHR);
                console.log('Error: ' + e);
            }
       });
  }

 function success_result() {
    location.reload();
  }
});