$(document).ready(function(){

    var headerInit={ "Accept": "application/json" ,
                "Content-Type": "application/json"}
    /*******Add college to scrollspy*********/
    function addScrollSpy(data) {
        $(".list_colleges").append("<a href='#college"+ data.id + "' class='name" + data.id +" list-group-item list-group-item-action' >" +data.name+ "</a>");
        $("#scrollspy-list").append("<div id = 'college" + data.id + "' class='mb-2'/>");
        
        /***********add info about college***********************/
        $("#college" + data.id).append("<div class='header' style='display: inline;'/>");
        $("#college" + data.id).append("<div class='body'/>");

        /***********add header to college********************/
        $("#college" + data.id + " .header").append("<h2 class = 'name'>"+data.name +"</h2>");
        $("#college" + data.id + " .header h2").append("<button type='button' class='edit btn btn-info ml-2' data-toggle='tooltip' data-placement='top' title='Edit college' id='edit" + data.id+ "' data-toggle='modal' data-target='#editModal'>Edit</button>");
        $("#college" + data.id + " .header h2").append("<button type='button' class='delete btn btn-info ml-2' id='delete" + data.id + "'>Delete</button>");

        /***********add body to college********************/
        $("#college" + data.id + " .body").append("<p class='description" + data.id + "' style='text-align: justify;'>" + data.description + "</p>");
        $("#college" + data.id + " .body").append("<div class='teachers'/>");        
        $("#college" + data.id + " .body .teachers").append("<h4 id='textTeacher"+ data.id +"'>List of the teachers</h4>");
        $("#college" + data.id + " .body .teachers h4").append("<button type='button' class='btnAddTeacher btn btn-info ml-2' name='" + data.id + "' data-toggle='tooltip' data-placement='top' title='Add teacher' id='btnAddTeacher" + data.id + "' data-toggle='modal' data-target='#addTeacher" + data.id +"'>Add teacher</button>");
        $("#college" + data.id + " .body").append("<hr class='my-4'/>");

        /************add cards of teachers**********************/
        $("#college" + data.id + " .teachers").append("<div class='container-fluid row'/>");
        if(data.teacherList.length != 0) {
            $.each(data.teacherList, function(){
                addTeacherCard(data.id, this);
            });
        } else {
            $("#college" + data.id + " .body .teachers").append("<p>The college doesn't have professors yet.</p>");
        }

        /*************add event to delete button*********************/
        $("#delete" + data.id).click(function() {
            ajaxSendObject("DELETE", "colleges/delete/"+data.id, null,null, null,successResult); 
        });

        /***********event to add teacher button ***************/
        $("#btnAddTeacher" + data.id).click(function(){
            $("#addTeacherModal").attr("name", this.name);
            $("#headerTeacherModal").text("Add new teacher to " + data.name);
            $("#addTeacherModal").modal("show");
        });

    /************add event to edit button**************/
    $("#edit" + data.id).click(function() {
        $('#editHeader').text("Edit information about " + data.name);
        $("#editName").attr("value", data.name);
        $("#editDescription").val(data.description);
        $('#editModal').attr("name", data.id);
        $('#editModal').modal('show');
    });

    }

    /*******Add teacher card*********/
    function addTeacherCard(id ,data) {
        $("#college" + id + " .teachers .container-fluid").append("<div class='col-sm-4 mb-1' id='teacher"+ data.id + "'/>");
        $("#teacher" + data.id).append("<div class='card'/>");
        $("#teacher" + data.id + " .card").append("<div class='card-block'/>");
        $("#teacher" + data.id + " .card-block").append("<h5 class='card-title'>" + data.name + "</h5>");
        $("#teacher" + data.id + " .card-block").append("<a href='teachers#teacher"+ data.id + "' class='btn btn-primary'>Learn more</a>");
    }

    /*******get data on load page**********/
    $("body").ready(function(){
        successGet = function(data) {
            $.each(data, function() {
                addScrollSpy(this);
            });
        }
        ajaxSendObject("GET", "colleges/list", null,null, null,successGet);
    });


    /*********edit college**************/
   $("#submitEdit").click(function(){
        var id = $('#editModal').attr("name");
        var editData = {"id" : id ,"name" : $("#editName").val(), "description" : $('#editDescription').val()};
        ajaxSendObject("POST", "colleges/edit/"+id, JSON.stringify(editData), headerInit, "html",successResult);
   });

    function successResult() {
        location.reload();
    }

    /***********add college*****************/
   $("#submitCollege").click(function(){
       var newData = {"name": $('#addName').val(), "description" : $('#addDescription').val(), "idCollege" : 1};
       ajaxSendObject("POST", "colleges/add", JSON.stringify(newData), headerInit, "html",successResult);
    });

    /************add teacher to college********************/
    $("#submitTeacher").click(function() {
        newTeacher = {"name" : $("#addTeacherName").val(), "description" : $("#addTeacherDescription").val(), "idCollege": $("#addTeacherModal").attr("name")};
         ajaxSendObject("POST", "teachers/add", newTeacher,null, 'html',successResult); 
   });

    function ajaxSendObject(type, url, data, headers,dataType, func) {
    $.ajax({
            type : type,
            url : url,
            headers:headers,
            data: data,
            dataType:  dataType,
            success : function(result) {
              console.log(type + " was successful.");
              func(result);
            },
            error : function(jqXHR, e) {
                console.log(jqXHR);
                console.log('Error: ' + e);
            }
       });
  }
});