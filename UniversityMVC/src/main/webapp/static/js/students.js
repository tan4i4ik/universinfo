$(document).ready(function(){

  headers = {
        "Accept": "application/json" ,
        "Content-Type": "application/json"
      }

  /********show student information***********************/
  function showStudent(data) {
    var i = 1;
    $("#infoStudent").append("<div id='student" + data.id + "'/>");
    $("#infoStudent #student" + data.id).append("<div class = 'header'/>");
    $("#infoStudent #student" + data.id +" .header").append("<div class = 'title row'/>");
    $("#infoStudent #student" + data.id + " .title").append("<h2>" + data.name + "</h2>");
    $("#infoStudent #student" + data.id + " .title").append($("<button type='button' class='btn btn-info ml-2 edit' name='" + data.id + "' id='editStudent" + data.id+ "'>Edit</button>"));
    $("#infoStudent #student" + data.id + " .title").append($("<button type='button' class='delete btn btn-info ml-2' name='" + data.id + "' id='delete" + data.id+ "'>Delete</button>"));
    $.each(data.subjects, function(){
      var divSubject = 'student' +data.id + '-course' + i;
      $("#infoStudent #student" + data.id).append('<div id="' + divSubject + '"/>');
      $("#" + divSubject).append("<h4>" + this.name + "</h4>");
      $("#" + divSubject).append("<table class='table table-bordered'/>");
      $("#" + divSubject + " table").append("<thead><tr><th scope='col'>#</th><th scope='col'>Topic name</th><th scope='col'>Grade</th><th scope='col'>Delete</th></tr></thead><tbody></tbody>");
      getSubjects("#" + divSubject + " table", this);
      $("#" + divSubject).append("<h5 style='position: absolute; right: 20px;'>Average grade: " + checkTotalScore("#" + divSubject + " table ") + "</h5>");
      $("#" + divSubject).append("<div style='padding-top:15px;'><hr class='my-4'/></div>");
      i++;
    });

    /*********delete student***************************/
    successDeleteStudent = function(result) {
      $("#infoStudent #student" + data.id).html("");
      $("#infoStudent #student" + data.id).append("<h5>" + data.name + " was deleted successful</h5>");
    }

    $(".delete").click(function(){
      var id = this.name;
      ajaxSendObject("DELETE", "students/delete/"+id, null,null, 'html', successDeleteStudent);

    });

    /*************edit student information********************/
    $("#editStudent" + data.id).click(function(){
      var name = $("#infoStudent #student" + data.id + " h2").text();
      $("#infoStudent #student" + data.id + " .title").hide();
      $("#infoStudent #student" + data.id + " .header").append("<form class = 'form-inline'/>");
      $("#infoStudent #student" + data.id + " .header form").append("<div class='form-group editStudent row'></div>");
      $("#infoStudent #student" + data.id + " .editStudent").append('<input type="text" class="form-control" id="editName' + data.id + '" value="' + data.name +'"/>' );
      $("#infoStudent #student" + data.id + " .editStudent").append("<button type='button' class='editSave btn btn-info ml-2' name='" + data.id + "' id='edit" + data.id+ "'>Save</button>");
      
      successEditStudent = function(){
        var editName = $("#editName" + data.id).val();
        $("#infoStudent #student" + data.id + " .header form").remove();
        $("#infoStudent #student" + data.id + " .title").show();   
        $("#infoStudent #student" + data.id + " .title h2").text(editName);
      }

      $(".editSave").click(function(){
        var editData = {"name":  $("#editName" + data.id).val()};
        ajaxSendObject("POST", "students/edit/"+data.id, JSON.stringify(editData), headers,null, successEditStudent);
      });
    });
    $("#infoStudent #student" + data.id).append("<hr class='my-5'></hr>");
  }

  /*********get subjects of student**********************/
  function getSubjects(id,data) {
    var i = 1;
    var totalScore = 0;
    $.each(data.topics, function(){
      var idGrade = this.id;
      $( id + " tbody").append("<tr scope='row' id='row" + i + "'/>");
      $( id + " tbody #row" + i).append("<th>" + i + "</th>");
      $( id + " tbody #row" + i).append("<td name='" + idGrade + "'>" + this.name + "</td>");
      $( id + " tbody #row" + i).append("<td class='grade'>" + this.grade + "</td>");
      $( id + " tbody #row" + i).append("<td><button type='button' class='btn btn-info ml-2' data-toggle='tooltip' data-placement='top' id='deleteGrade"+idGrade+"' >Delete</button></td>");
      
      /*******delete grade of student***********************/
      $("#deleteGrade" + idGrade).click(function(){

        successDelete = function() {
          var tr = '#row' + idGrade;
          $(tr).remove();
        }
        ajaxSendObject("DELETE", "students/delete/graded/" + idGrade, null, null,null, successDelete);
      });
      i++;
    });
  }

  /*********check average grade on course **********************/
  function checkTotalScore(idTable) {
    var result = 0, i = 0;

    $(idTable + " tbody").find('tr').each(function(m){
      var curr = parseInt($(this).find('.grade').text());
      result += curr;
      i++;

    });
    return Math.round(result/i * 100) / 100;

  }

/*********search students in db ********************/
  $("#search").click(function(event){
    $("#infoStudent").html("");
    event.preventDefault();
    name = $("#searchStudentName").val();

    successGetStudent = function(data) {
      $("#searchStudentName").val("");
      console.log("Result successful.");
      if(data.length != 0) {
        $.each(data, function(){
          showStudent(this);
        });
      } else {
        $("#infoStudent").append("<h4>Student wasn't found.</h4>");
      }  
    } 
    ajaxSendObject("GET", "students/get", {"name" : name},null, null, successGetStudent);
  });

  /******add student****************/
  $("#submit").click(function(){
    var data = {"name": $('#addName').val()};
    ajaxSendObject("POST","students/add", JSON.stringify(data), headers, null, successResult);
    });

  /******ajax************************/
  function ajaxSendObject(type,url,data,headers, dataType, func) {
    $.ajax({
      type : type,
      url : url,
      data: data,
      headers:headers,
      dataType:  dataType,
      success : function(result) {
        console.log(type + " was successful.");
        func(result);
      },
      error : function(jqXHR, e) {
        console.log(jqXHR);
        console.log('Error: ' + e);
      }
    });
  }

 function successResult() {
    location.reload();
  }
});
