$(document).ready(function(){
    var x = Math.floor((Math.random() * 8) + 1);
    var card = "",
        list = "",
        path ="";
    for(i = 1; i <=x; i++) {
        card ="<div class='col-sm-3 mb-5'>" +
                 "<div class='card'>" +
                     "<img class='card-img-top img-fluid' src='static/js/articles/photo" + i + ".jpg' alt='Articles photo' />"+
                     "<div class='card-block'>" +
                         "<h3 class='card-title'>New article</h3>" +
                         "<p class='card-text'>New article about university.</p>" +
                         "<a href='#' class='btn btn-primary'>Learn more</a>" +
                     "</div>" +
                 "</div>" +
              "</div>";
        list += card;
    }
    $(".row").append(list);
});
  