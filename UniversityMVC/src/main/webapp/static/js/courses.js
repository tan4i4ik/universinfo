$(document).ready(function(){

  var flag = false;
  var flagEditTopic = false;

  /*******Add subjects to scrollspy*********/
  function addScrollSpy(data) {
    $(".list_subjects").append("<a href='#course"+ data.id + "' class='name" + data.id +" list-group-item list-group-item-action' ><b>" +data.name+ "</b></a>");
    $("#scrollspy-list").append("<div id = 'course" + data.id + "' class='mb-2'/>");

    /***********add info about course***********************/
    $("#course" + data.id).append("<div class='header row'/>");
    $("#course" + data.id).append("<div class='body'/>");

    /***********add header of course********************/
    $("#course" + data.id + " .header").append("<h2 class = 'name'>"+data.name +"</h2>");
    $("#course" + data.id + " .header").append("<button type='button' class='btn btn-info ml-2' data-toggle='tooltip' data-placement='top' title='Edit course' id='editCourse" + data.id+ "' data-toggle='modal' data-target='#editSubjectModal'>Edit</button>");
    $("#course" + data.id + " .header").append("<button type='button' class='btn btn-info ml-2' id='deleteCourse" + data.id + "'>Delete</button>");
    
    /***********add body of course********************/
    $("#course" + data.id + " .body").append("<p class='teacher" + data.id + "' style='text-align: justify;'><i>" + "Professor's name: " + "<a href = 'teachers#teacher" + data.teacher_id +"'>" + data.teacher_name + "</a></i></p>");
    $("#course" + data.id + " .body").append("<p class='description" + data.id + "' style='text-align: justify;'>" + data.description + "</p>");
    $("#course" + data.id + " .body").append("<div id='topicList" + data.id + "' class='list-group'/>");
    $("#course" + data.id + " .body").append("<button id='btnAddTopic" +data.id + "' type='button' class='btn btn-info mt-2 addTopic' data-toggle='modal' data-target='#addModalTopic' name='" +data.id + "'>New Topic</button>");
    $("#course" + data.id + " .body").append("<hr class='my-4'/>");

    /*******show each topic in course**********/
    $.each(data.topics, function(){
      var name = this.name;
      var description = this.description;
      var students = this.students;
      $("#course" + data.id + " #topicList" + data.id).append("<button id='btnTopic" + this.id + "' class='list-group-item list-group-item-action' data-toggle='modal' data-target='#showTopic' name='" + this.id + "'>" +this.name + "</button>");  

      /******open topic with modal info***********/
      $("#btnTopic" +this.id).click(function(){
        /*******clean show modal *****************/
        $("#btnEditTopic").text("Edit topic");
        $("#lblStudentList").text("");
        $("#showTopic table").html("");

        /***********get topic information to modal*****************/
        $("#topicLabel").text(name);
        $("#topicDescription").text(description);
        $('#showTopic').attr("name", this.name);

        /******show students grade on current topic ******************/
        if(students.length != 0) { 
          $("#lblStudentList").text("Students Graded List");
          printTable("#showTopic",students);
        }
        $('#showTopic').modal('show');
      });
    });

    /*********delete course************************/
    $("#deleteCourse" + data.id).click(function(){
       ajaxSendObject("DELETE","courses/delete/"+data.id,null,null,successResult);
    });

    /*********open modal for edit course******************/
    $("#editCourse" + data.id).click(function() {
      $('#editHeader').text("Edit information about " + data.name);
      $("#editName").val(data.name);
      $("#editDescription").val(data.description);
      $('#editSubjectModal').attr("name", data.id);
      $('#editSubjectModal').modal('show');
    });

    /********add topic to course*******************/
    $("#btnAddTopic" +data.id).click(function(){
      $("#titleAddTopicModal").text("Add new topic of " + data.name);
      $('#addTopicModal').attr("name", data.id);
      $('#addTopicModal').modal('show');
    });

  }

  /***********print table on modal****************************/
  function printTable(id,data) { 
    var i = 1;
    $(id + " table").append("<thead/>");
    $(id + " table thead").append("<tr/>");
    $(id + " table thead tr").append("<th scope='col'>#</th>");
    $(id + " table thead tr").append("<th scope='col'>Name </th>");
    $(id + " table thead tr").append("<th scope='col'>Graded</th>");
    $(id + " table thead tr").append("<th scope='col'>Delete</th>");
    $(id + " table").append("<tbody/>");
      $.each(data, function(){
        idGrade = this.id;
        $(id + " table tbody").append("<tr class='row" + i + "'/>");
        $(id + " table tbody .row" + i).append("<th scope = 'row'>" + i + "</th>");
        $(id + " table tbody .row" + i).append("<td>" + this.name + "</td>");
        $(id + " table tbody .row" + i).append("<td>" + this.graded + "</td>");
        $(id + " table tbody .row" + i).append("<td><button type='button' class='btn btn-info ml-2' id='deleteGrade"+this.id+"' >Delete</button></td>");
         
          /********delete grade of student from topic******************/
          $("#deleteGrade" + this.id).click(function(){
            ajaxSendObject("DELETE","courses/delete/graded/"+idGrade,null,null,successResult);
          });
         i++;
      });
  }

  function isEmpty(data) {
    var flag = true;
    $.map(data, function(value, key) {
      flag = false;
    });
    return flag;
  }

  /*******get data on load page**********/
  $("body").ready(function(){
    /**********get teachers*****************/
    successGetTeachers = function(data) {
      if(isEmpty(data)) {
        $('#addButton').prop('disabled', true);
        $('#tooltipAddBtn').attr('tabindex','0');
        $('#tooltipAddBtn').attr('data-toggle','tooltip');        
      } else {
        $.map(data, function(value, key) {
          $(".teachersSelect").append("<option id=teacher'"+key+"' name='" + key + "'>" + value + "</option>");
        });
      }
    }

    ajaxSendObject("GET","courses/teachers", null, "json",successGetTeachers);

    /********get courses**************/
    var successShowCourses = function(data) {
      $.each(data, function() {
        var id = this.id;
        addScrollSpy(this);
      });
    };

    ajaxSendObject("GET", "courses/list", null, "json", successShowCourses);
  });

  /*********add course to db*****************/
  $("#submitAddCourse").click(function(){
    var teacher = $("#addTeachers option:selected").attr("name");
    var data = {"name": $('#addCourseName').val(), "description" : $('#addCourseDescription').val(), "idTeacher" : teacher };
    ajaxSendObject("POST","courses/add", data, 'html', successResult);
  });

  /**********edit course by id******************/
  $("#submitEdit").click(function(){
    var id = $('#editSubjectModal').attr("name");
    var teacher = $("#editTeachers option:selected").attr("name");
    var data = {"name" : $("#editName").val(), "description" : $('#editDescription').val(), "idTeacher" : teacher};
    ajaxSendObject("POST", "courses/edit/"+ id, data, 'html', successResult);
  });

  /*********add topic to course********************/
  $("#submitAddTopic").click(function(){
    var id = $('#addTopicModal').attr("name");
    var data = {"name" : $("#addTopicName").val(), "description" : $("#addTopicDescription").val(), "id" : id};
    ajaxSendObject("POST", "topics/add", data,'html', successResult);
  });

  /*********delete topic from course******************************/
  $("#btnDeleteTopic").click(function(){
    var id = $('#showTopic').attr("name");
    ajaxSendObject("DELETE", "topics/delete/" + id, null,'html', successResult);
  });

  /**********add student and add student to model**************************/
  $("#btnAddStudent").click(function(){
    var idTopic = $("#showTopic").attr("name");
    if (flag == false) {
      flag = true;
      $("#addStudent").append("<div class='addName form-group'/>");
      $("#addStudent .addName").append("<input type='text' class='form-control' id='addStudentName' placeholder='Input student\'s name'/>");
      $("#addStudent").append("<div class='addGrade form-group'/>");
      $("#addStudent .addGrade").append("<input type='text' class='form-control' id='addStudentGrade' placeholder='Input student\'s grade'/>");
      $("#addStudent").append("<div class='addStudent form-group'/>");
      $("#addStudent .addStudent").append("<button type='submit' class='btn btn-primary' id='btnAddStudent'>Submit</button>")

      $("#btnAddStudent").click(function(){
          addData = {"name" : $("#addStudentName").val(), "grade" : $("#addStudentGrade").val()};
          ajaxSendObject("POST", "courses/add/topic/" + idTopic + "/add_student", addData,'html', successResult);
        });
    } else {
      $("#addStudent").html("");
      flag = false;
    }
  });

  /*********add info to topic for editing****************************************/
  $("#btnEditTopic").click(function(){
    if(flagEditTopic == false) {
      flagEditTopic = true;
      var topicName = $("#showTopic h5").text();
      var topicDescription = $("#showTopic #topicDescription").text();
      $("#showTopic h5").html("");
      $("#showTopic h5").append("<input type='text' class='form-control' id='editTopicName' value='" + topicName + "'/>");
      $("#showTopic #topicDescription").html("");
      $("#showTopic #topicDescription").append("<textarea class='form-control' id='editTopicDescription' style='width:200%;height:50%'></textarea>");
      $("#showTopic #editTopicDescription").text(topicDescription);
      $("#btnEditTopic").text("Save edit");
    }else{
      flagEditTopic = false;
      $("#btnEditTopic").text("Edit topic");
      var idTopic = $("#showTopic").attr("name");
      data = {"name" : $("#editTopicName").val(), "description" : $("#editTopicDescription").val()};
      ajaxSendObject("POST", "topics/edit/" + idTopic, data,'html', successResult);
    }
  });

  function ajaxSendObject(type,url,data,dataType, func) {
    $.ajax({
      type : type,
      url : url,
      data: data,
      dataType:  dataType,
      success : function(result) {
        console.log(type + " was successful.");
        func(result);
      },
      error : function(jqXHR, e) {
        console.log(jqXHR);
        console.log('Error: ' + e);
      }
    });
  }

 function successResult() {
    location.reload();
  }
});