$(document).ready(function(){
  /*******Add college to scrollspy*********/
  function addScrollSpy(data) {
    $(".list_teachers").append("<a href='#teacher"+ data.id + "' class='name" + data.id +" list-group-item list-group-item-action'><b>" +data.name+ "</b></a>");
    $("#scrollspy-list").append("<div id = 'teacher" + data.id + "' class='mb-2'></div>");
       
    /***********add info about teacher***********************/
    $("#teacher" + data.id).append("<div class='header'/>");
    $("#teacher" + data.id).append("<div class='body'/>");

    /***********add header of teacher********************/
    $("#teacher" + data.id + " .header").append("<h2 class = 'name'>"+data.name +"</h2>");
    $("#teacher" + data.id + " .header h2").append("<button type='button' class='btn btn-info ml-2' data-toggle='tooltip' data-placement='top' title='Edit teacher' id='edit" + data.id+ "' data-toggle='modal' data-target='#editModal'>Edit</button>");
    $("#teacher" + data.id + " .header h2").append("<button type='button' class='btn btn-info ml-2' id='deleteTeacher" + data.id + "'>Delete</button>");
       
    /*******add body of teacher*********/
    $("#teacher" + data.id + " .body").append("<p class = 'teacher" + data.id + "'><i>Professor is part of <a href = 'colleges#college" + data.college_id +"'>" + data.college_name + "</a></i></p>");
    $("#teacher" + data.id + " .body").append("<p class='description" + data.id + "' style='text-align: justify;'>" + data.description + "</p>");
    $("#teacher" + data.id + " .body").append("<div class='subjects'/>");

    $("#teacher" + data.id + " .body").append("<hr class='my-4'/>");
    $("#teacher" + data.id + " .subjects").append("<h4 id='textCourses"+ data.id +"'>List of the courses</h4>");
    $("#teacher" + data.id + " .subjects h4").append("<button type='button' class='btn btn-info ml-2' name='" + data.id + "' data-toggle='tooltip' data-placement='top' title='Add course' id='btnAddCourse" + data.id + "' data-toggle='modal' data-target='#addCourse" + data.id +"'>Add course</button>");
    $("#teacher" + data.id + " .subjects").append("<div class='container-fluid row'/>");
    if(data.list.length != 0) {
      $.each(data.list, function(){
        addSubjectCard(data.id, this);
      });
    } else {
      $("#teacher" + data.id + " .body .subjects").append("<p>The professors doesn't have classes yet.</p>");
    }

    /******delete teacher********/
    $("#deleteTeacher" + data.id).click(function() {
      ajaxSendObject("DELETE", "teachers/delete/"+data.id, null,null, null,successResult); 
    });

    /*********open modal for edit teacher*****************/
    $("#edit" + data.id).click(function(){
      $('#editHeader').text("Edit information about " + data.name);
      $("#editName").val(data.name);
      $("#editDescription").val(data.description);
      $('#editModal').attr("name", data.id);
      $('#editModal').modal('show');
    });

    /***********open modal for add course********************/
    $("#btnAddCourse" + data.id).click(function(){
      $("#addCourseHeader").text("Add new course of " + data.name);
      $('#addCourse').attr("name", data.id);
      $('#addCourse').modal('show');
    });
  }

  function isEmpty(data) {
    var flag = true;
    $.map(data, function(value, key) {
      flag = false;
    });
    return flag;
  }

  /*******Add group card*********/
  function addSubjectCard(id ,data) {
    $("#teacher" + id + " .container-fluid").append("<div class='col-sm-4 mb-1' id='subject"+ data.id + "'></div>");
    $("#subject" + data.id).append("<div class='card'></div>");
    $("#subject" + data.id + " .card").append("<div class='card-block'></div>");
    $("#teacher" + id + " #subject" + data.id + " .card-block").append("<h5 class='card-title'>" + data.name + "</h5>");
    $("#teacher" + id + " #subject" + data.id + " .card-block").append("<a href='courses#course" + data.id + "' class='btn btn-primary'>Learn more</a>");
  }

  /*******get data on load page**********/
  $("body").ready(function(){
    /**********get colleges*****************/
    successGetColleges = function(data) {
      if(isEmpty(data)) {
        $('#addButton').prop('disabled', true);
        $('#tooltipAddBtn').attr('tabindex','0');
        $('#tooltipAddBtn').attr('data-toggle','tooltip');      
      } else {
          $.map(data, function(value, key) {
            $(".collegesSelect").append("<option id=college'"+key+"' name='" + key + "'>" + value + "</option>");
        });
      } 
    }

    ajaxSendObject("GET","teachers/colleges", null, null,"json",successGetColleges);
    
    /******get teachers*********************/
    successGetTeachers = function(data) {
      $.each(data, function() {
        addScrollSpy(this);
      });
    }
    
    ajaxSendObject("GET","teachers/list", null, null,"json",successGetTeachers);
  });

  /*********edit teacher*************************/
  $("#submitEditTeacher").click(function(){
    var id = $('#editModal').attr("name");
    var college = $("#collegesEdit option:selected").attr("name");
    var data = {"name" : $("#editName").val(), "description" : $('#editDescription').val(), "idCollege" : college};
    ajaxSendObject("POST","teachers/edit/"+ id, data, null,null,successResult);
  });

  /*********add teacher*************************/
  $("#submitAddTeacher").click(function(){
    var college = $("#collegesAdd option:selected").attr("name");
    var data = {"name": $('#addName').val(), "description" : $('#addDescription').val(), "idCollege" : college};
    ajaxSendObject("POST","teachers/add", data, null,null,successResult);
  });

  /*********add course*************************/
  $("#submitAddCourse").click(function(){
    var dataCourse = {"name" : $("#courseName").val(), "description" :  $("#courseDescription").val(), "idTeacher":$("#addCourse").attr("name")};
    ajaxSendObject("POST","courses/add", dataCourse, null,null,successResult);
  });

  function ajaxSendObject(type, url, data, headers,dataType, func) {
    $.ajax({
      type : type,
      url : url,
      headers:headers,   
      data: data,
      dataType:  dataType,
      success : function(result) {
        console.log(type + " was successful.");
        func(result);
      },
      error : function(jqXHR, e) {
        console.log(jqXHR);
        console.log('Error: ' + e);
      }
    });
  }


  function successResult() {
    location.reload();
  }
});
