$(document).ready(function(){
    $(".signIn").on('keyup', function(){
        var regexMail = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{1,4})+$/;
        var regexPass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})/;
        var login = $('#registrationLogin').val();
        var fullName = $('#fullName').val();
        var mail = $('#e-mail').val();
        var password = $('#registrationPassword').val();
        var confirmPassword = $('#confirmPassword').val();

        var validationPass = regexPass.test(password);
        var validationMail = regexMail.test(mail);

        /****validation for password, mail, confirm mail******/
        if(!validationPass) {
             $('#messagePass').html("The password must contains one lower case letter, one upper case letter and one number and six symbols").css('color', 'red');
        } else {
            $('#messagePass').html("");
        }

        if(!validationMail) {
             $('#emailComment').html("The mail doesn't look right").css('color', 'red');
        }else {
            $('#emailComment').html("");
        }

        if(password != confirmPassword) {
            $('#messageConfPass').html("The password isn't same").css('color', 'red');
        }   else {
             $('#messageConfPass').html("");
        }

        if( ( login != 0) && ( fullName != 0 ) && validationPass && (password == confirmPassword) && validationMail) {
            $('#submitRegistration').prop('disabled', false);
        } else {
            $('#submitRegistration').prop('disabled', true);
        }
    });
});
  