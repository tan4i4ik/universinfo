<nav class="navbar  navbar-toggleable-md navbar-light bg-faded ">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href = "/university/main">
        <img src="static/css/image/emblem.png" width="30" height="30" class="d-inline-block align-top" alt="">University
    </a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="colleges">Colleges</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="teachers">Teachers</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="courses">Courses</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="students">Students</a>
            </li>
        </ul>
        <a href="<c:url value="/logout" />"><button type="button" id="signOut" class="btn btn-info">Logout</button></a>
    </div>
</nav>