<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <%@include file="static.jsp"%>
        <script src="<c:url value="/static/js/students.js" />"></script>
    </head>
    <body>
        <main>
            <%@include file="navbar.jsp"%>
            <div class="mt-4">
                <div class="container-fluid">
                    <div class="row">
                        <div data-spy="affix" class = "col-sm-2">
                            <div id="navbar-example" class="list-group sticky-top list_students"></div>
                            <div id="idAddButton">
                                <button type="button" class="btn btn-info mt-2" data-toggle="modal" data-target="#addModal">New student</button>
                            </div>
                        </div>
                        <div  class = "col-sm-10">
                            <div id = "search_student">
                                <form class = "form-inline">
                                    <div class="form-group row">
                                        <div class="col">
                                            <label for="searchStudentName">Search student</label>
                                        </div>
                                        <div class="col">
                                            <input type="text" class="form-control" id="searchStudentName" placeholder="Input student's name..."/>
                                        </div>
                                        <div class="col">
                                            <button type="submit" id="search" class="btn btn-primary col">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="infoStudent"></div>
                        </div>
                    </div>
                    <!--Add student modal-->
                    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">New student</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="addName" class="form-control-label" placeholder="Input student's name...">Student name: </label>
                                            <input type="text" class="form-control" id="addName"/>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary" id="submit" data-dismiss="modal">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <footer class="footer">
            <p class="text-right mr-md-3">The project was developed by Tetiana Siagailo. Git: <a href = "https://gitlab.com/tan4i4ik">https://gitlab.com/tan4i4ik</a></p>
        </footer>
    </body>
</html>