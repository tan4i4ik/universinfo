<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <%@include file="static.jsp"%>
        <script src="<c:url value="/static/js/courses.js" />"></script>
    </head>
    <body>
        <main>
            <%@include file="navbar.jsp"%>
            <div class="mt-4">
                <div class="container-fluid">
                    <div class="row">
                        <div data-spy="affix" class = "col-sm-3">
                            <div id="navbar-example" class="list-group sticky-top list_subjects"></div>
                            <div id="idAddButton">
                                <span id="tooltipAddBtn" title="The university must have colleges and teachers for creating new courses">
                                    <button type="button" class="btn btn-info mt-2" data-toggle="modal" data-target="#addModal" id="addButton">New course</button>
                                </span>
                            </div>
                        </div>
                        <div class = "col-sm-9">
                            <div id="scrollspy-list" data-spy="scroll" data-target=".list_subjects"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Add course modal-->
            <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add course</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="addCourseName" class="form-control-label">The name of the course:</label>
                                    <input type="text" class="form-control" id="addCourseName"/>
                                </div>
                                <div class="form-group">
                                    <label for="addCourseDescription" class="form-control-label">Description of the course:</label>
                                    <textarea class="form-control" id="addCourseDescription"></textarea>
                                </div>
                                <div class="form-group" id="select">
                                    <select class="form-control teachersSelect" id="addTeachers" data-live-search="true"></select>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="submitAddCourse" data-dismiss="modal">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--Edit subject information-->
            <div class="modal fade" id="editSubjectModal" tabindex="-1" role="dialog" aria-labelledby="editHeader" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editHeader"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="editName" class="form-control-label">Name of subject:</label>
                                    <input type="text" class="form-control" id="editName"/>
                                </div>
                                <div class="form-group">
                                    <label for="editDescription" class="form-control-label">Description of subject:</label>
                                    <textarea class="form-control" id="editDescription" text = "hello" style="height:150px;"></textarea>
                                </div>
                                <div class="form-group" id="selectEdit">
                                    <select class="form-control teachersSelect" id="editTeachers" data-live-search="true"></select>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="submitEdit" data-dismiss="modal">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--Show topic information-->
            <div class="modal fade" id="showTopic" tabindex="-1" role="dialog" aria-labelledby="showHeader" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="topicLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label id="topicDescription" class="form-control-label"></label>
                                </div>
                                <div class="form-group">
                                    <label id="lblStudentList" for="studentsList" class="form-control-label" ></label>
                                    <table id="students" class="table table-bordered"></table>
                                </div>
                                <div id="addStudent" class="form-group"></div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnAddStudent" class = "btn btn-info" >Add student</button>
                            <button type="button" class="btn btn-info ml-2" data-toggle="tooltip" data-placement="top" id="btnEditTopic">Edit topic</button>
                            <button type="button" class="btn btn-info ml-2" data-toggle="tooltip" data-placement="top" id="btnDeleteTopic">Delete topic</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--Add topic information-->
            <div class="modal fade" id="addTopicModal" tabindex="-1" role="dialog" aria-labelledby="addModal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="titleAddTopicModal"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="addTopicName" class="form-control-label">Name of topic:</label>
                                    <input type="text" class="form-control" id="addTopicName"/>
                                </div>
                                <div class="form-group">
                                    <label for="addTopicDescription" class="form-control-label">Description of topic:</label>
                                    <textarea class="form-control" id="addTopicDescription" style="height:150px;"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="submitAddTopic" data-dismiss="modal">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <footer class="footer">
            <p class="text-right mr-md-3">The project was developed by Tetiana Siagailo. Git: <a href = "https://gitlab.com/tan4i4ik">https://gitlab.com/tan4i4ik</a></p>
        </footer>
    </body>
</html>