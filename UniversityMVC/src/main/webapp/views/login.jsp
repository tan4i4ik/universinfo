<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
    <head>
        <%@include file="static.jsp"%>
    </head>
    <body>
        <main>
            <nav class="navbar navbar-toggleable-md navbar-light bg-faded mb-1">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href = "#">
                    <img src="static/css/image/emblem.png" width="30" height="30" class="d-inline-block align-top" alt="">University
                </a>
            </nav>
            <div class="jumbotron index_img m">
                <h1 class="display-3">Welcome to university</h1>
                <hr class="my-4"/>
                <p>If you didn't create account before, go to registration page. If you have your account, you could login to get all information about our university.</p>
            </div>
            <div id ="authForm">
                <h3>Log in</h3>
                <c:if test="${not empty error}">
                    <div class="message">${error}</div>
                </c:if>
                <c:if test="${not empty message}">
                    <div class="message">${message}</div>
                </c:if>
                <form name='loginForm' action="<c:url value='/login' />" method='POST'>
                    <div class="form-group">
                        <label for="username" class="form-control-label">Username:</label>
                        <input type="text" class="form-control" id="username" name="username"/>
                    </div>
                    <div class="form-group">
                        <label for="password" class="form-control-label">Password:</label>
                        <input type="password" class="form-control" id="password" name="password"/>
                    </div>
                    <div class="form-group">
                        <a id="reg" class = "text-right font-weight-bold text-primary" name="registration" href="registration">Registration</a>
                        <input id="submit" class = "btn btn-primary btn-lg float-right " name="submit" type="submit" value="Submit" />
                    </div>
                </form>
                <c:if test="${not empty sessionScope.message}">
                    <span style="color:green"><c:out value="${sessionScope.message}"/></span>
                    <c:remove var="message" scope="session" />
                </c:if>
            </div>
        </main>
        <footer class="footer">
            <p class="text-right mr-md-3">The project was developed by Tetiana Siagailo. Git: <a href = "https://gitlab.com/tan4i4ik">https://gitlab.com/tan4i4ik</a>  </p>
        </footer>
    </body>
</html>