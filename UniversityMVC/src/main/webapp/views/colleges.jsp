<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <%@include file="static.jsp"%>
        <script src="<c:url value="/static/js/colleges.js" />"></script>
    </head>
    <body>
        <main>
            <%@include file="navbar.jsp"%>
            <div class="mt-4 pb-3">
                <div class="container-fluid">
                    <div class="row">
                        <div data-spy="affix" class = "col-sm-3">
                            <div id="navbar-example" class="list-group sticky-top list_colleges" >
                            </div>
                        <div id="idAddButton">
                            <button type="button" class="btn btn-info mt-2" data-toggle="modal" data-target="#addCollegeModal">Add college</button>
                        </div>
                    </div>
                    <div class = "col-sm-9" >
                        <div id="scrollspy-list" data-spy="scroll" data-target=".list_colleges" >
                        </div>
                    </div>
                </div>
            </div>
            <!--Add college modal-->
            <div class="modal fade" id="addCollegeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add college</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="addName" class="form-control-label">College name: </label>
                                    <input type="text" class="form-control" id="addName"/>
                                </div>
                                <div class="form-group">
                                    <label for="addDescription" class="form-control-label">Description of the college:</label>
                                    <textarea class="form-control" id="addDescription"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="submitCollege" data-dismiss="modal">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

            <!--Edit college information-->
            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editHeader" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editHeader"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="editName" class="form-control-label">Name of college:</label>
                                    <input type="text" class="form-control" id="editName"/>
                                </div>
                                <div class="form-group">
                                    <label for="editDescription" class="form-control-label">Description of college:</label>
                                    <textarea class="form-control" id="editDescription" style="height:150px;"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="submitEdit"data-dismiss="modal">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

            <!--Add teacher modal-->
            <div class="modal fade" id="addTeacherModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="headerTeacherModal"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="addTeacherName" class="form-control-label">Teacher name: </label>
                                    <input type="text" class="form-control" id="addTeacherName"/>
                                </div>
                                <div class="form-group">
                                    <label for="addTeacherDescription" class="form-control-label">Description of the teacher:</label>
                                    <textarea class="form-control" id="addTeacherDescription"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="submitTeacher" data-dismiss="modal">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <footer id="footer" class="footer">
            <p class="text-right mr-md-3">The project was developed by Tetiana Siagailo. Git: <a href = "https://gitlab.com/tan4i4ik">https://gitlab.com/tan4i4ik</a></p>
        </footer>
    </body>
</html>