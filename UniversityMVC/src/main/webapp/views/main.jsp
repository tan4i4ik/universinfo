<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:th="http://www.thymeleaf.org" 
    xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3"
    xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout">
    <head>
        <%@include file="static.jsp"%>
        <script src="<c:url value="/static/js/main.js" />"></script>
    </head>
    <body>
        <main>
            <%@include file="navbar.jsp"%>
            <div class="jumbotron index_img">
                <h1 class="display-3">Welcome to university</h1>
                <hr class="my-4"/>
                <p>Here you can have access to creating, deleting, updating data about colleges, teachers, courses, topics and students. </p>
            </div>
            <div class="row articles"></div>
        </main>
        <footer class="footer">
            <p class="text-right mr-md-3">The project was developed by Tetiana Siagailo. Git: <a href = "https://gitlab.com/tan4i4ik">https://gitlab.com/tan4i4ik</a>  </p>
        </footer>
    </body>
</html>
