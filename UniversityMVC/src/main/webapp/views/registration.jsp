<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page session="true"%>
<html>
    <head>
        <%@include file="static.jsp"%>
        <script src="<c:url value="/static/js/registration.js" />"></script>
    </head>
    <body>
        <main>
            <nav class="navbar navbar-toggleable-md navbar-light bg-faded mb-1">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href = "#">
                    <img src="static/css/image/emblem.png" width="30" height="30" class="d-inline-block align-top" alt="">University
                </a>
            </nav>
            <div class="jumbotron index_img">
                <h1 class="display-3">Welcome to university</h1>
                <hr class="my-4"/>
                <p>If you didn't create account before, go to registration page. If you have your account, you could login to get all information about our university.</p>
            </div>
            <div id ="authForm">
                <h3>Registration</h3>
                <c:if test="${not empty result}">
                    <div class="alert alert-info" role="alert">${result}</div>
                </c:if>
                <form name='registrationForm'  action="<c:url value='/registration' />" method='POST'>
                    <div class="form-group">
                        <label for="registrationLogin" class="form-control-label">Login<sup>*</sup></label>
                        <input type="text" class="form-control signIn" id="registrationLogin" name="username" required>
                    </div>
                    <div class="form-group">
                        <label for="fullName" class="form-control-label">Full name<sup>*</sup></label>
                        <input type="text" class="form-control signIn" id="fullName" name="fullName" required>
                    </div>
                    <div class="form-group">
                        <label for="email" class="form-control-label">E-mail<sup>*</sup></label>
                        <input type="email" class="form-control signIn" name= "email" id="e-mail" aria-describedby="emailHelp" required/>
                        <small id="emailComment"></small>
                    </div>
                    <div class="form-group">
                        <label for="password" class="form-control-label">Password<sup>*</sup></label>
                        <input type="password" class="form-control signIn" name="password" id="registrationPassword" required>
                        <small id="messagePass"></small>
                    </div>
                    <div class="form-group">
                        <label for="confirmPassword" class="form-control-label">Confirm password<sup>*</sup></label>
                        <input type="password" class="form-control signIn" id="confirmPassword" data-match="#registrationPassword" required>
                        <small id="messageConfPass"></small>
                    </div>
                    <div class="form-group">
                        <a id="login" class = "text-right font-weight-bold text-primary" name="login" href="login">Return to login</a>
                        <input id="submitRegistration" class = "btn btn-primary btn-lg float-right" name="submit" type="submit" value="Submit" />
                    </div>
                </form>
            </div>
        </main>    
        <footer class="footer">
            <p class="text-right mr-md-3 ">The project was developed by Tetiana Siagailo. Git: <a href = "https://gitlab.com/tan4i4ik">https://gitlab.com/tan4i4ik</a></p>
        </footer>
    </body>
</html>
